import { Component, OnInit } from '@angular/core';
import { MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-terms-dialog',
  templateUrl: './terms-dialog.component.html'
})
export class TermsDialogComponent implements OnInit {


	constructor(public dialogRef: MatDialogRef<TermsDialogComponent>) {
  }

	ngOnInit() {

  }
	closeDialog(): void {
	   	this.dialogRef.close();
	}
}
