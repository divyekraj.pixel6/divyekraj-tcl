import { Component, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-error-404',
  templateUrl: './error-404.component.html'
})

export class Error404Component implements OnDestroy {
  error: any;
  navigationSubscription: Subscription;

  constructor(private router: Router, public route: ActivatedRoute) {

    this.navigationSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        console.log(event);
        if (event.urlAfterRedirects.indexOf('/session-expired') !== -1) {
          this.error = 0;
        } else if (event.urlAfterRedirects.indexOf('/400') !== -1) {
          this.error = 400;
        } else if (event.urlAfterRedirects.indexOf('/401') !== -1) {
          this.error = 401;
        } else if (event.urlAfterRedirects.indexOf('/403') !== -1) {
          this.error = 403;
        } else if (event.urlAfterRedirects.indexOf('/500') !== -1) {
          this.error = 500;
        } else {
          this.error = 404;
        }
      }
    });
  }
  ngOnDestroy() {
    this.navigationSubscription.unsubscribe();
  }
}
