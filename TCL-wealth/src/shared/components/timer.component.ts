import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { timer } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-spinner',
  template: `<span *ngIf="pattern!='spinner' && displayValue" class="countdown-timer">
              {{prefix}} <span>{{ displayValue }}</span> {{ displayUnit }}
             </span>
             <div *ngIf="pattern=='spinner'" #circle class="progress-circle"><span>{{progressPercent}}</span></div>
             `,
  styles: [`
.progress-circle {
  position: relative;
  display: inline-block;
  margin: 1rem;
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background-color: #ebebeb; 
}

.progress-circle:after {
  content: '';
  display: inline-block;
  width: 100%;
  height: 100%;
  border-radius: 50%;
  -webkit-animation: colorload 2s;
  animation: colorload 2s; 
}

.progress-circle span {
  font-size: 12px;
  color: #8b8b8b;
  position: absolute;
  left: 50%;
  top: 50%;
  display: block;
  width: 40px;
  height: 40px;
  line-height: 40px;
  margin-left: -20px;
  margin-top: -20px;
  text-align: center;
  border-radius: 50%;
  background: #fff;
  z-index: 1;
}

.progress-circle span:after {
  content: "%";
  font-weight: 600;
  color: #8b8b8b; 
}
.progress-circle:after {
  background-image: linear-gradient(90deg, #66b8ff 50%, transparent 50%, transparent), linear-gradient(270deg, #66b8ff 50%, #ebebeb 50%, #ebebeb); 
}

  `]
})
export class TimerComponent {
  @Output() private onChange = new EventEmitter<any>();
  @Input() public prefix;
  @Input() public pattern;
  @Input() private timeout;
  @Input() private percentage;
  @Input() public type; /*
                    1 =  time in SS
                    2 = time in MM:SS
                    3 = time in HH:MM:SS
                   */
  private timeLapsed;
  private intrvl: any;
  public displayValue: any;
  public displayUnit: string;

  public isTimerRunning: boolean;

  progressPercent: any;
  
  constructor() {
    this.isTimerRunning = false;
    this.prefix = '';
    this.type = 1;
    this.percentage = false;
    this.timeout = 120;
    this.pattern = 'text';
  }
  ngOnInit(){
 }
  public startOtpTimer() {
console.log(this.type, this.pattern);
    if(this.intrvl) this.intrvl.unsubscribe();
    this.isTimerRunning = true;

    if(this.type == 3){
      this.displayUnit = "hours";
      this.timeout = this.timeout * 3600;
    } else if(this.type == 2){
      this.displayUnit = "minutes";
      this.timeout = this.timeout * 60;
    } else {
      this.displayUnit = "seconds";
    }

    this.intrvl = timer(1000, 1000).pipe(
          map(i =>  this.timeout - i),
          take(this.timeout + 1))
    .subscribe(i => {

              this.timeLapsed = i;
              if(this.percentage){
                this.progressPercent = ((this.timeout - this.timeLapsed) * 100) / this.timeout;
              }
              if(this.type == 1){
                this.displayValue = (0-this.timeLapsed);
              }else if(this.type == 2){
                //const minutes: number = Math.floor(this.timeLapsed/60);
                //this.displayValue = ('00'+ minutes).slice(-2) + ':' + ('00'+Math.floor(this.timeLapsed-minutes * 60)).slice(-2);
                this.displayValue = moment.utc(this.timeLapsed * 1000).format("mm:ss");
              }else if(this.type == 3){
                //const hrs: number = Math.floor(this.timeLapsed/3600);
                //const minutes: number = Math.floor((this.timeLapsed%3600)/60);
                //this.displayValue = ('00'+ hrs).slice(-2) + ':' + ('00'+ minutes).slice(-2) + ':' + ('00'+Math.floor(this.timeLapsed-minutes * 60)).slice(-2);
                this.displayValue = moment.utc(this.timeLapsed * 1000).format("hh:mm:ss");

              }
      console.log(i, this.displayValue, this.progressPercent);

              if(!i) {
                  this.intrvl.unsubscribe();
                  this.isTimerRunning = false;
                  this.onChange.emit(this.isTimerRunning);
              }
      });
  }
  ngOnDestroy() {
    if(this.intrvl) { this.intrvl.unsubscribe(); }
  }
}
