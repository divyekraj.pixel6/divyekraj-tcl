import { Component, Input } from '@angular/core';

/**
 * The component displayed in the confirmation modal opened by the ConfirmService.
 *
 *  TYpe 0- small and absolute (Use in form fields ONLY)
 *  Type 1- loaderCenter
 *  TYpe 2- fullscreen
 */
@Component({
  selector: 'app-loader',
  template: `<div class="loading-wrap"  [class.inline]="type == 0" [class.loaderCenter]="type == 1" [class.fullscreen]="type == 2">
                  <div class="loading-icon">
                      <img alt="Loading..." src="assets/images/img_loading.gif"/>
                  </div>
              </div>`,
    styles: [`

        .loading-wrap {
            text-align: center;
            vertical-align: middle;

        }
        .loading-wrap.inline {
            display: inline-block;
            
        }

        .loading-icon {
            padding: 0 20px;
        }
        .loading-wrap.inline .loading-icon {
            padding: 0;
        }
        .loading-icon img {
            width: 25px;
            height: 25px;
        }
        .loading-icon.small {
            padding: 0;
            position: absolute;
            top: 10px;
            right: 5px;
        }
        .loading-icon.small img {
            width: 20px;
            height: 20px;
        }
        .fullscreen {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 99;
            height: 100vh;
            display: flex;
            align-items: center;
            justify-content: center;
        }

    `]

})
export class LoaderComponent{
	@Input() type: any;
	constructor(){
		this.type = 1;
	}
}
