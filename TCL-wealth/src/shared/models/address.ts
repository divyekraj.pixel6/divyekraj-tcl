import * as moment from 'moment';


export class Address {
    address1: string;
    address2: string;
    pincode: string;
    state: string;
    city: string;
	  country: string;

    constructor(jsonObj) {
        if (jsonObj) {
            this.address1 = jsonObj.address1 ? jsonObj.address1 : '';
            this.address2 = jsonObj.address2 ? jsonObj.address2 : '';
            this.pincode = jsonObj.pincode ? jsonObj.pincode : '';
            this.state = jsonObj.state ? jsonObj.state : '';
            this.city = jsonObj.city ? jsonObj.city : '';

			this.country = jsonObj.country ? jsonObj.country : '';
        } else {
            this.address1 = '';
            this.address2 = '';
            this.pincode = '';
            this.state = '';
            this.city = '';
			      this.country = '';
        }
    }
}

export class DMYDateFormater {

    date: any = {
        "year": '',
        "month": '',
        "day": ''
    };

    constructor(dateString) {
        if (dateString && typeof dateString == 'string') {
            this.format(dateString);
        } else if (dateString && typeof dateString == 'object') {
            this.date.year = dateString.year;
            this.date.month = dateString.month;
            this.date.day = dateString.day;
        }
    }

    format(date) {
      
        if (date && typeof date == 'string') {
             //DD-MM-YYYY
            let dateArray: any = date.split('-');
            let dateObj: any;
            if (dateArray[2] && dateArray[1] && dateArray[0]) {
                dateObj = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);
            } else {
                //DD/MM/YYYY
                let dateArray: any = date.split('/');
                if (dateArray[2] && dateArray[1] && dateArray[0]) {
                    dateObj = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);
                } else {
                    //DD MM YYYY
                    let dateArray: any = date.split(' ');
                    if (dateArray[2] && dateArray[1] && dateArray[0]) {
                        dateObj = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);
                    }
                }
            }
            if (dateObj) {
                this.date.year = dateObj.getFullYear();
                this.date.month = dateObj.getMonth() + 1;
                this.date.day = dateObj.getDate();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}
