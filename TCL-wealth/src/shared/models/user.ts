export class User
{
    appId: string;
    firstName: string;
    middleName: string;
    lastName: string;
    mobileNumber: string;
    email: string;
    existingUser: number;

    constructor(jsonObj){
	    if(jsonObj){
            //customer app fields
            this.appId = jsonObj.appId ? jsonObj.appId.toString() : '';
            this.firstName = jsonObj.firstName ? jsonObj.firstName : '';
            this.middleName = jsonObj.middleName ? jsonObj.middleName : '';
            this.lastName = jsonObj.lastName ? jsonObj.lastName : '';
            this.mobileNumber = jsonObj.mobileNumber ? jsonObj.mobileNumber : '';
            this.email = jsonObj.email ? jsonObj.email : '';
            this.existingUser = jsonObj.existingUser ? jsonObj.existingUser : 0;

	    }else {
            this.appId = '';
            this.firstName = '';
            this.middleName = '';
            this.lastName = '';
            this.mobileNumber = '';
            this.email = '';
            this.existingUser = 0;
	    }
	}
}
