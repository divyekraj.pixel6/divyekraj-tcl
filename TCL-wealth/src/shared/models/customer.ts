import { Address, DMYDateFormater } from './address';

function convertFloatString(value) {

    const input = value ? value : 0;
    const parsed = parseFloat(input).toFixed(2);
    // console.log(value, parsed);
    return parsed;
}
function convertIntString(value) {

    const input = value ? value : 0;
    const parsed = parseFloat(input).toFixed(0);
    // console.log(value, parsed);
    return parsed;
}


export class LoanPuprposes{
	items: any;
	constructor(jsonObj) {
		if (jsonObj) {

		    this.items =  new Array();
		    if(jsonObj && jsonObj.length){
		        jsonObj.forEach(item => {
		            this.items.push(new LoanPuprpose(item));
		        }, this);
		    }
		}
	}
}
export class LoanPuprpose {
	key: string;
	value: string;
	class: string;
  id: string;

	constructor(jsonObj) {
		if (jsonObj) {
            this.key = jsonObj.key ? jsonObj.key : '';
            this.value = jsonObj.value ? jsonObj.value : '';
            this.class = '';

			let key = this.key.toString();
			switch(key){
				case '1': this.class = 'business'; break;
				case '2': this.class = 'capital'; break;
				case '3': this.class = 'wedding'; break;
				case '4': this.class = 'travel'; break;
				case '5': this.class = 'medical'; break;
				case '6': this.class = 'renovation'; break;
				case '7': this.class = 'education'; break;
				case '8': this.class = 'occasion'; break;
				case '9': this.class = 'purchase'; break;
				case '10': this.class = 'debt'; break;

			}
		}
	}
}
// TellUs model start

export class PersonalDetails {
    dob: DMYDateFormater;
    gender: string;
    currentAddress: string;
    currentCity: string;
    currentState: string;
    currentPinCode: string;
    currentCountry: string;
    residentialStatus: string;
    currentAddressIs: string;
    citizenShip: string;
    maritalStatus: string;
    educationQualification: string;
    currentAddressProof: string;
    currentAddressProofNumber: string;
    noOfDependents: string;
    currentAddrNoOfYears: any;
    currentAddrNoOfMonths: any;
    aadhaaradressproof: any;
	constructor(jsonObj) {
		if (jsonObj) {
            this.dob = jsonObj.dob ? new DMYDateFormater(jsonObj.dob) : null;
            this.gender = jsonObj.gender ? jsonObj.gender : '';
            this.currentAddress = jsonObj.currentAddress ? jsonObj.currentAddress : '';
            this.currentCity = jsonObj.currentCity ? jsonObj.currentCity : '';
            this.currentState = jsonObj.currentState ? jsonObj.currentState : '';
            this.currentPinCode = jsonObj.currentPinCode ? jsonObj.currentPinCode : '';
            this.currentCountry = jsonObj.currentCountry ? jsonObj.currentCountry : '';
            this.residentialStatus = jsonObj.residentialStatus ? jsonObj.residentialStatus : '';
            this.currentAddressIs = jsonObj.currentAddressIs ? jsonObj.currentAddressIs : '';
            this.citizenShip = jsonObj.citizenShip ? jsonObj.citizenShip : '';
            this.maritalStatus = jsonObj.maritalStatus ? jsonObj.maritalStatus : '';
            this.educationQualification = jsonObj.educationQualification ? jsonObj.educationQualification : '';
            this.currentAddressProof = jsonObj.currentAddressProof ? jsonObj.currentAddressProof : '';
            this.currentAddressProofNumber = jsonObj.currentAddressProofNumber ? jsonObj.currentAddressProofNumber : '';
            this.noOfDependents = jsonObj.noOfDependents ? jsonObj.noOfDependents : '';
            this.currentAddrNoOfYears = jsonObj.currentAddrNoOfYears == 0 ? jsonObj.currentAddrNoOfYears : jsonObj.currentAddrNoOfYears;
            this.currentAddrNoOfMonths = jsonObj.currentAddrNoOfMonths == 0 ? jsonObj.currentAddrNoOfMonths : jsonObj.currentAddrNoOfMonths;
            this.aadhaaradressproof = jsonObj.aadhaaradressproof ? jsonObj.aadhaaradressproof : '';
		} else {
            this.dob = null;
            this.gender = '';
            this.maritalStatus = '';
            this.educationQualification = '';
            this.currentAddress = '';
            this.currentCity = '';
            this.currentState = '';
            this.currentPinCode = '';
            this.currentCountry = '';
            this.currentAddressIs = '';
            this.citizenShip = '';
            this.residentialStatus = '';
            this.currentAddressProof = '';
            this.currentAddressProofNumber = '';
            this.noOfDependents = '';
            this.currentAddrNoOfYears = '';
            this.currentAddrNoOfMonths = '';
            this.aadhaaradressproof = '';
		}
	}
}


//Professional Detail
export class ProfessionalDetail {
    organisationType: string;
    monthlyTakeHomeSalary: string;
    monthlyObligations: string;
    expCurrentOrganisation: string;
    totalExp: string;   
    constructor(jsonObj) {
        if (jsonObj) {
            this.organisationType = jsonObj.organisationType ? jsonObj.organisationType : '';
            this.monthlyTakeHomeSalary = jsonObj.monthlyTakeHomeSalary ? jsonObj.monthlyTakeHomeSalary : '';
            this.monthlyObligations = jsonObj.monthlyObligations ? jsonObj.monthlyObligations : '0';
            this.expCurrentOrganisation = jsonObj.expCurrentOrganisation ? jsonObj.expCurrentOrganisation : '';
            this.totalExp = jsonObj.totalExp ? jsonObj.totalExp : '';
        } else {
            this.organisationType = '';
            this.monthlyTakeHomeSalary = '';
            this.monthlyObligations = '';
            this.expCurrentOrganisation = '';
            this.totalExp = '';
        }
    }
}


//initial Offer
export class InitialOfferDetails {
    loanAmount: string;
    tenure: string;
    insurancePolicyFlag: string;
    rateOffIntrest: string;
    mothlyEMI: string;
    isNTCCustomer: string;
    thankYouPage: string;
    revisedOfferDisable: string;
	constructor(jsonObj) {
		if (jsonObj) {
            this.loanAmount = jsonObj.loanAmount ? jsonObj.loanAmount : '';
            this.tenure = jsonObj.tenure ? jsonObj.tenure : '';
            this.insurancePolicyFlag = jsonObj.insurancePolicyFlag ? jsonObj.insurancePolicyFlag : '';
            this.rateOffIntrest = jsonObj.rateOffIntrest ? jsonObj.rateOffIntrest : '';
            this.mothlyEMI = jsonObj.mothlyEMI ? jsonObj.mothlyEMI : '';
            this.isNTCCustomer = jsonObj.isNTCCustomer ? jsonObj.isNTCCustomer : '';
            this.thankYouPage = jsonObj.thankYouPage ? jsonObj.thankYouPage : '';
            this.revisedOfferDisable = jsonObj.revisedOfferDisable ? jsonObj.revisedOfferDisable : '';
		} else {
            this.loanAmount = '';
            this.tenure = '';
            this.insurancePolicyFlag = '';
            this.rateOffIntrest = '';
            this.mothlyEMI = '';
            this.isNTCCustomer = '';
            this.thankYouPage = '';
            this.revisedOfferDisable = '';
		}
	}
}

