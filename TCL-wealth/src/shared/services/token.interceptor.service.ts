import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, from, Subject, throwError } from 'rxjs';
import { mergeMap, switchMap, catchError } from "rxjs/operators";

import { UserService } from './user.service';
import { Cache } from './cache.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    userService: UserService;
    cache: Cache;
    tokenRefreshedSource = new Subject();
    tokenRefreshed$ = this.tokenRefreshedSource.asObservable();
    refreshTokenInProgress = false;

    constructor(private injector: Injector,
        private router: Router) {
    }

    addAuthHeader(request) {
        if (this.cache.user.authorization) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${this.cache.user.authorization}`
                }
            });
        }
        return request;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        this.userService = this.injector.get(UserService);
        this.cache = this.injector.get(Cache);
        // Handle request
        if (!request.url.includes("generate-token")) {
            request = this.addAuthHeader(request);
        }
        return next.handle(request).pipe(
            catchError(error => {
                if (error.status === 401) {
                    return this.refreshToken().pipe(
                        mergeMap(() => {
                            request = this.addAuthHeader(request);
                            return next.handle(request);
                        }),
                        catchError(error => {
                            //this.userService.logout();
                            //this.router.navigateByUrl('');
                            return throwError(error);
                        })
                    );
                }
                return throwError(error);
            })
        );
    }

    refreshToken() {
        if (this.refreshTokenInProgress) {
            return new Observable(observer => {
                this.tokenRefreshed$.subscribe(() => {
                    observer.next();
                    observer.complete();
                });
            });
        } else {
            this.refreshTokenInProgress = true;
            return from(this.userService.generateToken()).pipe(
                switchMap(() => {
                    this.refreshTokenInProgress = false;
                    this.tokenRefreshedSource.next();
                    return new Observable(observer => {
                        observer.next();
                        observer.complete();
                    });
                }),
                catchError(error => {
                    //this.userService.logout();
                    //this.router.navigateByUrl('');
                    return throwError(error);
                })
            );
        }
    }

    // intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    //       this.userService = this.injector.get(UserService);
    //       this.cache = this.injector.get(Cache);
    //       // Handle request
    //
    //       if (!request.url.includes("generate-token")) {
    //       request = this.addAuthHeader(request);
    //       }
    //
    //       return next.handle(request).pipe(
    //       catchError( error => {
    //       console.log('error', error);
    //
    //       if (error.status === 401) {
    //       if (request.url.includes("generate-token")) {
    //       this.userService.logout();
    //       }
    //
    //       }
    //       return throwError(error);
    //       })
    //       );
    //  }

}
