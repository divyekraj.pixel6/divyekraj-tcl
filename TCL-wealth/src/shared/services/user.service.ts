import { Injectable, Inject } from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import { Router } from '@angular/router';

import { Cache } from './cache.service';
import { APP_CONFIG } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';

import { ApiService } from './api.service';

@Injectable()
export class UserService {

    appConfig: IAppConfig;
    userIdleWatchStarted: boolean;
    allowThirdPartyRedirection: boolean;

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
        private cache: Cache,
        public apiService: ApiService,
        private router: Router,
        private userIdle: UserIdleService) {

        this.appConfig = appConfig;
        this.cache.loggedIn = false;
        this.allowThirdPartyRedirection = false;
        this.userIdleWatchStarted = false;

        this.userIdle.onTimerStart().subscribe(count => {
            console.log(count);
        });

        this.userIdle.onTimeout().subscribe(() => {
            console.log("Timeout");
            this.userIdle.stopWatching();
            this.userIdleWatchStarted = false;
            this.router.navigate(["application", "thank-you"]);
        });
    }

    isAuthenticated() {
        let user = this.cache.user;
        if (user && user.applicationId && user.applicationId != '0') {
            console.log('isAuthenticated...YES');
            this.cache.loggedIn = true;
            this.startUserSession();
            return true;
        } else {
            return false;
        }
    }

    logout() {
        this.cache.clear('user');
        this.cache.user = {};
        this.cache.loggedIn = false;
        this.stopUserSession();
        console.log("***** logout *****");
    }

    startUserSession() {
        if (!this.userIdleWatchStarted) {
            console.log("#####...Started Watching...#####");
            this.userIdle.startWatching();
            this.userIdleWatchStarted = true;
        }
    }

    stopUserSession() {
        if (this.userIdleWatchStarted) {
            this.userIdle.stopWatching();
        }
    }

    stopUserIdleTimer() {
        if (this.userIdleWatchStarted) {
            this.userIdle.stopTimer();
        }
    }

    isRedirectionAllowed() {
        return this.allowThirdPartyRedirection;
    }

    setRedirectionAllowed(what) {
        this.allowThirdPartyRedirection = what;
    }

    generateToken() {
        this.cache.user.authorization = '';
        this.cache.set('user', this.cache.user);
        return new Promise((resolve, reject) => {
            let data = { mobileNumber: this.cache.user.mobileNumber };
            this.apiService.postApi(this.appConfig.endpoints.generateToken, data, null).then((resp: any) => {
                if (resp && resp.status && resp.statusMessage) {
                    if (resp.statusMessage && resp.statusMessage.toLowerCase() == 'success') {
                        this.cache.loggedIn = true;
                        this.cache.user.authorization = resp.accessToken;
                        this.cache.set('user', this.cache.user);
                        resolve(resp);
                    } else {
                        reject(resp.statusMessage ? resp.statusMessage : this.apiService.commonStrings.http_error);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    getOtp(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getOtp, data, null)
                .then((resp: any) => {
                    if (resp) {
                        if (resp && resp.statusCode && resp.statusCode == 200) {
                            resolve(resp);
                        } else {
                            reject(resp.status ? resp.status : this.apiService.commonStrings.http_error);
                        }
                    } else {
                        reject(this.apiService.commonStrings.http_error);
                    }
                }, error => {
                    console.log('Oooops!' + error);
                    reject(this.apiService.commonStrings.http_error);
                });
        });
    }

    verifyOtp(data) {
        return new Promise((resolve, reject) => {

            this.apiService.postApi(this.appConfig.endpoints.verifyOtp, data, null)
                .then((resp: any) => {
                    if (resp) {
                        if (resp && resp.statusCode && resp.statusCode == 200) {
                            resolve(resp);
                        } else {
                            reject(resp.status ? resp.status : this.apiService.commonStrings.http_error);
                        }
                    } else {
                        reject(this.apiService.commonStrings.http_error);
                    }
                }, error => {
                    console.log('Oooops!' + error);
                    reject(this.apiService.commonStrings.http_error);
                });
        });
    }

}
