import { Injectable, Inject } from '@angular/core';

import { APP_CONFIG } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';

import { ApiService } from './api.service';

@Injectable()
export class EvokeService {

    appConfig: IAppConfig;

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
                public apiService: ApiService) {

        this.appConfig = appConfig;

    }

    gstinVerify(gstin) {
        let data = { "gstin": gstin };
        return this.apiService.postApiCancellable(this.appConfig.endpoints.gstInfo, data, null);
    }

    getDetailsByZipCode(postalCode) {
        let data = { "ZipCode": postalCode, "applicationId": 0 };
        return this.apiService.postApiCancellable(this.appConfig.lookups.getDetailsByZipCode, data, null);
    }


    getDetailsByPan(pan) {
        let data = { "panNo": pan };
        return this.apiService.postApiCancellable(this.appConfig.endpoints.getDetailsByPan, data, null);
    }

    verifyCaptcha(data) {
        return this.apiService.postApiCancellable(this.appConfig.endpoints.getMcaInfo, data, null);
    }
}
