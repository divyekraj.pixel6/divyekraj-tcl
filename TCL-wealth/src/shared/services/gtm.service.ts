import { Injectable, Inject } from '@angular/core';
import { WINDOW } from 'ngx-window-token';
import { environment } from '../../environments/environment';

@Injectable()
export class GTMService {

    _window: any;

    constructor(@Inject(WINDOW) _window) {
        this._window = _window;
        this._window.dataLayer = this._window.dataLayer || [];
        this.debug();
    }

    sendEvent(event) {
        if (environment.production && this._window.dataLayer) {
            console.log('sending GTM event-', event);
            this._window.dataLayer.push(event);
        }
    }
    
    debug() {
        if (!environment.production){
            console.log(this._window.dataLayer);
        }
    }
}
