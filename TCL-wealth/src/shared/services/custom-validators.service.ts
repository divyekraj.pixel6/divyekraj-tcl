import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class CustomValidators {
    
    static fullNameValidator(maxlength) {
        return (control: FormControl) => {
            const fullName = control.value;
            if (fullName) {
                if (fullName.length > maxlength) {
                    return {
                        maxlength: true
                    };
                } else {
                    const re = new RegExp(/^[A-Za-z\s]+$/);
                    if (!re.test(fullName)) {
                        return {
                            pattern: true
                        };
                    }
                }
            }
            return null;
        };
    }

    static companyNameValidator(maxlength) {
        return (control: FormControl) => {
            const compName = control.value;
            const re = new RegExp(/^[A-Za-z0-9\.\,\&\(\)\-\s]+$/);
            if (compName) {
                if (compName.length > maxlength) {
                    return {
                        maxlength: true
                    };
                } else if (!re.test(compName)) {
                    return {
                        pattern: true
                    };
                }
            }
            return null;
        };
    }

    static panValidator(control: FormControl) {
        const pan = control.value;
        const re = new RegExp(/^[A-Z]{3}[P][A-Z][0-9]{4}[A-Z]$/);
        if (pan) {
            if (pan.length > 10) {
                return {
                    maxlength: true
                };
            } else if (!re.test(pan)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static companyPersonalPanValidator(control: FormControl) {
        const pan = control.value;
        const re = new RegExp(/^[A-Z]{3}[ABCEFGHJLPT][A-Z][0-9]{4}[A-Z]$/);
        if (pan) {
            if (pan.length > 10) {
                return {
                    maxlength: true
                };
            } else if (!re.test(pan)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static companyPanValidator(control: FormControl) {
        const pan = control.value;
        const re = new RegExp(/^[A-Z]{3}[ABCEFGHJLT][A-Z][0-9]{4}[A-Z]$/);
        if (pan) {
            if (pan.length > 10) {
                return {
                    maxlength: true
                };
            } else if (!re.test(pan)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    
    static cityValidator(maxlength) {
        return (control: FormControl) => {
            const city = control.value;
            const re = new RegExp(/^[A-Za-z\.\,\-\s]+$/);
            if (city) {
                if (city.length > maxlength) {
                    return {
                        maxlength: true
                    };
                } else if (!re.test(city)) {
                    return {
                        pattern: true
                    };
                }
            }
            return null;
        };
    }

    static addressValidator(maxlength) {
        return (control: FormControl) => {
            const address = control.value;
            const re = new RegExp(/^[0-9a-zA-Z.,;:\[\](){}\/\\'\-\s]*$/);

            if (address) {
                if (address.length > maxlength) {
                    return {
                        maxlength: true
                    };
                } else if (!re.test(address)) {
                    return {
                        pattern: true
                    };
                }
            }
            return null;
        };
    }

    static postalCodeValidator(control: FormControl) {
        const postal = control.value;
        const re = new RegExp(/^[0-9]{6}$/);
        if (postal) {
            if (postal.length > 6) {
                return {
                    maxlength: true
                };
            } else if (!re.test(postal)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static mobileValidator(control: FormControl) {
        const mobile = control.value;
        const re = new RegExp(/^[6-9]{1}[0-9]{9}$/);
        if (mobile) {
            if (mobile.length > 10) {
                return {
                    maxlength: true
                };
            } else if (!re.test(mobile)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static emailValidator(control: FormControl) {
        const email = control.value;
        const re = new RegExp(/^[a-zA-Z0-9]([a-zA-Z0-9_-]|(\.(?!\.)))+[a-zA-Z0-9]\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,})+$/);
        if (email) {
            if (email.length > 255) {
                return {
                    maxlength: true
                };
            } else if (!re.test(email)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static gstinValidator(control: FormControl) {
        const selector = control.value;
        const re = new RegExp(/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/);
        if (selector) {
            if (selector.length > 15) {
                return {
                    maxlength: true
                };
            } else if (!re.test(selector)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

   static electricityBillValidator(control: FormControl) {
        const electricityBill = control.value;
        const re = new RegExp(/^[0-9]{10}$/);
        if (electricityBill) {
            if (electricityBill.length > 10) {
                return {
                    maxlength: true
                };
            } else if (!re.test(electricityBill)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static aadharValidator(control: FormControl) {
        const aadhar = control.value;
        const re = new RegExp(/^[0-9]{12}$/);
        if (aadhar) {
            if (aadhar.length > 12) {
                return {
                    maxlength: true
                };
            } else if (!re.test(aadhar)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static voterCardValidator(control: FormControl) {
        const voterCard = control.value;
        const re = new RegExp(/^[a-zA-Z]{3}[0-9]{7}$/);
        if (voterCard) {
            if (voterCard.length > 10) {
                return {
                    maxlength: true
                };
            } else if (!re.test(voterCard)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static passportValidator(control: FormControl) {
        const passport = control.value;
        const re = new RegExp(/^[a-zA-Z]{1}[a-zA-Z0-9]{7,14}$/);
        if (passport) {
            if (passport.length > 15) {
                return {
                    maxlength: true
                };
            } else if (!re.test(passport)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }

    static currencyValidator(control: FormControl) {
        const currency = control.value;
        const re = new RegExp(/^[1-9]{1}[0-9]+$/);
       if (currency) {
            if (currency.length > 7) {
                return {
                    maxlength: true
                };
            } else if (!re.test(currency)) {
                return {
                    pattern: true
                };
            }
        }
        return null;
    }
}

/**
 * Custom ErrorStateMatcher which returns true (error exists) when the parent form group is invalid and the control has been touched
*/
export class ConfirmValidParentMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return (control.parent.invalid && (control.touched || isSubmitted));
    }
}

export class FormSubmittedMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return (control.invalid && (control.touched || isSubmitted));
    }
}
