import { Injectable, Inject } from '@angular/core';

import { APP_CONFIG } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';

import { ApiService } from './api.service';
import { UserService } from './user.service';
import { Cache } from './cache.service';
import { IndianCurrencyPipe } from '../pipe/currency.pipe';

import { LoanPuprposes } from '../models/customer';

@Injectable()
export class ConstantsService {

  appConfig: IAppConfig;
  constants: any;

  constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
    public apiService: ApiService,
    public userService: UserService,
    public cache: Cache,
    public indianCurrencyPipe: IndianCurrencyPipe) {

    this.appConfig = appConfig;

    this.constants = {
      customerCategories: [],
      states: [],
      cities: [],
      maritalStatus: [],
      citizenShip: [],
      education: [],
      ownedBy: [],
      residentialStatus: [],
      gender: [],
      addressProof: [],
      employment: [],
      occupations: [],
      industry: [],
      subIndustry: [],
      years: [],
      months: [],

      loanTypes: [],
      loanDuration: [{ id: 1, value: 'Days' }, { id: 2, value: 'Months' }],
      yesNo: [{ id: 1, value: 'Yes' }, { id: 2, value: 'No' }],
      noOfDependents: [{ id: 1, value: "0" }, { id: 2, value: "1" },{ id: 3, value: "2" }, { id: 4, value: "3" },{ id: 5, value: "4" }, { id: 6, value: "4+" }],
      organisationType: [{id: 1, value:'Public Ltd'}, {id: 2, value:'Private Ltd'}, {id: 3, value:'Proprietary'}, {id: 4, value:'NGO'}],
    };


    for(let i=1; i<=30; i++){
      this.constants.years.push({ id: i, value: i });
    }
    for(let i=1; i<=12; i++){
      this.constants.months.push({ id: i, value: i });
    }
  }

  addNumbers(numbers) {
    let total: any = 0;
    numbers.forEach((n) => {
      total += n ? Number(parseFloat(this.indianCurrencyPipe.parse(n)).toFixed(2)) : 0;
    });

    total = total ? parseFloat(total).toFixed(2) : 0.00;
    return total;
  }

  subNumbers(number1, number2) {
    let total: any = 0;

    number1 = number1 ? Number(parseFloat(this.indianCurrencyPipe.parse(number1)).toFixed(2)) : 0;
    number2 = number2 ? Number(parseFloat(this.indianCurrencyPipe.parse(number2)).toFixed(2)) : 0;

    total = number1 - number2;
    total = total ? parseFloat(total).toFixed(2) : 0.00;
    return total;
  }
  divideNumbers(number1, number2) {
    let total: any = '';

    number1 = number1 ? Number(parseFloat(this.indianCurrencyPipe.parse(number1)).toFixed(2)) : 0;
    number2 = number2 ? Number(parseFloat(this.indianCurrencyPipe.parse(number2)).toFixed(2)) : 0;
    if (number2 != 0) {
      total = number1 / number2;
      total = total ? parseFloat(total).toFixed(2) : 0.00;
    }
    return total;
  }
  addInteger(numbers) {
    let total: any = 0;
    numbers.forEach((n) => {
      n = parseInt(n);
      total += n ? n : 0;
    });
    total = total ? parseInt(total) : 0;
    return total;
  }
  subInteger(number1, number2) {
    let total: any = 0;

    number1 = number1 ? parseInt(number1) : 0;
    number2 = number2 ? parseInt(number2) : 0;

    total = number1 - number2;
    total = total ? parseInt(total) : 0;
    return total;
  }

  /* Customer lookpup API */

  getLookupByName(lookup) {
    if (lookup == this.appConfig.customerlookups.employment) {
            if (this.constants.employment && this.constants.employment.length > 0) { return; }
        } else if (lookup == this.appConfig.customerlookups.residentialStatus) {
            if (this.constants.residentialStatus && this.constants.residentialStatus.length > 0) { return; }
        } else if (lookup == this.appConfig.customerlookups.ownedBy) {
            if (this.constants.ownedBy && this.constants.ownedBy.length > 0) { return; }
        } else if (lookup == this.appConfig.customerlookups.maritalStatus) {
            if (this.constants.maritalStatus && this.constants.maritalStatus.length > 0) { return; }
        } else if (lookup == this.appConfig.customerlookups.education) {
            if (this.constants.education && this.constants.education.length > 0) { return; }
        } else if (lookup == this.appConfig.customerlookups.addressProof) {
            if (this.constants.addressProof && this.constants.addressProof.length > 0) { return; }
        } else if (lookup == this.appConfig.customerlookups.gender) {
            if (this.constants.gender && this.constants.gender.length > 0) { return; }
        }  else if (lookup == this.appConfig.customerlookups.citizenShip) {
            if (this.constants.citizenShip && this.constants.citizenShip.length > 0) { return; }
        }


    const data = { 'lookupName': lookup};

    return new Promise((resolve, reject) => {
      this.apiService.postApi(this.appConfig.lookups.lookupByName, data, null)
        .then(resp => {
          if (resp) {
              if (lookup == this.appConfig.customerlookups.employment) {
                  this.constants.employment = resp;
              } else if (lookup == this.appConfig.customerlookups.residentialStatus) {
                  this.constants.residentialStatus = resp;
              } else if (lookup == this.appConfig.customerlookups.ownedBy) {
                  this.constants.ownedBy = resp;
              } else if (lookup == this.appConfig.customerlookups.maritalStatus) {
                  this.constants.maritalStatus = resp;
              } else if (lookup == this.appConfig.customerlookups.education) {
                  this.constants.education = resp;
              } else if (lookup == this.appConfig.customerlookups.addressProof) {
                  this.constants.addressProof = resp;
              } else if (lookup == this.appConfig.customerlookups.gender) {
                  this.constants.gender = resp;
              } else if (lookup == this.appConfig.customerlookups.citizenShip) {
                  this.constants.citizenShip = resp;
              }
          }
          resolve(resp);
        }, error => {
          reject(error);
        });
    });
  }

  getPurposeOfLookUp() {
    if (this.constants.loanTypes && this.constants.loanTypes.length > 1) { return; }
    return new Promise((resolve, reject) => {

      const data = { 'applicationId': 0, categoryName: this.appConfig.customerlookups.loanPurpose};
      this.apiService.postApi(this.appConfig.lookups.loanTypes, data, null)
        .then((resp: any) => {
           let lookupObj= new LoanPuprposes(resp);
           this.constants.loanTypes = lookupObj.items;
          resolve(true);
        }, error => {
          reject(error);
        });
    });
  }

  getIndustry(employmentType) {
        return new Promise((resolve, reject) => {
            let data = { "employmentType": employmentType };
            this.apiService.postApi(this.appConfig.lookups.getIndustry, data, null).then((resp: any) => {
                this.constants.industry[employmentType] = resp;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
  }

  getSubIndustry(industryKey) {
       return new Promise((resolve, reject) => {
           let data = { "industryKey": industryKey };
           this.apiService.postApi(this.appConfig.lookups.getSubIndustry, data, null).then((resp: any) => {
               this.constants.subIndustry[industryKey] = resp;
               resolve(true);
           }, (error) => {
               reject(error);
           });
       });
   }

   getOccupationOrProfile(subIndustryKey) {
        return new Promise((resolve, reject) => {
            let data = { "subIndustryKey": subIndustryKey };
            this.apiService.postApi(this.appConfig.lookups.getSubindustryOccuption, data, null).then((resp: any) => {
                this.constants.occupations[subIndustryKey] = resp;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    banks() {
          if (this.constants.bankName && this.constants.bankName.length > 1) return;
          return new Promise((resolve, reject) => {
              this.apiService.getApi(this.appConfig.lookups.getBankName, null, null).then((resp: any) => {
                  this.constants.bankName = resp;
                  resolve(true);
              }, (error) => {
                  reject(error);
              });
          });
    }

   getCities() {

    //if (this.constants.cities && this.constants.cities.length > 1) { return; }
    return new Promise((resolve, reject) => {

      const data = { 'applicationId': 0 };
      this.apiService.getApi(this.appConfig.lookups.cities, data, null)
        .then(resp => {
          this.constants.cities = resp;
          resolve(true);
        }, error => {
          reject(error);
        });
    });
  }

  getStates() {

    //if (this.constants.states && this.constants.states.length > 1) { return; }
    return new Promise((resolve, reject) => {

      const data = { 'applicationId': 0 };
      this.apiService.getApi(this.appConfig.lookups.states, data, null)
        .then((resp: any) => {
          this.constants.states = resp;
          resolve(true);
        }, error => {
          reject(error);
        });
    });
  }

  convertKeyToString(resp) {
    let out = new Array();
    if (resp && resp.length) {
      resp.forEach(item => {
        if (item && item.key && item.value) {
          out.push({ 'key': item.key.toString(), 'value': item.value });
        }
      });
    } else {
      out = [];
    }
    return out;
  }

}
