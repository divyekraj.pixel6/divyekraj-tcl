import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError as observableThrowError, Observable, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { APP_CONFIG } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';

import { Cache } from './cache.service';

@Injectable()
export class ApiService {

    appConfig: IAppConfig;

    private actionSource = new Subject<any>();
    currentAction = this.actionSource.asObservable();
    commonStrings: any;
    downloadDisabled: boolean;

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
        private http: HttpClient,
        private cache: Cache) {

        this.appConfig = appConfig;

        this.commonStrings = {
            no_access: 'Please login to complete the application',
            http_error: 'Internal server error',
            download_error: 'Unable to download PDF...',
            data_saved: 'Data saved',
            data_updated: 'Data Updated',
            file_size: 'Please upload file size below 10MB',
            file_extention: 'Allowed file types are PDF, PNG, JPEG, JPG'
        };
        this.downloadDisabled = false;
    }

    sendAction(message: any) {
        this.actionSource.next(message);
    }

    isDropOff(resp) {
        if (resp && resp.statusCode &&
            resp.statusCode != 200 &&
            resp.statusCode != 704 &&
            resp.statusCode != 706 &&
            resp.statusCode != 715 &&
            resp.statusCode != 719 &&
            resp.statusCode != 727 &&
            resp.statusCode != 734 &&
            resp.statusCode != 795 &&
            resp.statusCode != 999) {
            return true;
        }
        return false;
    }

    isDropOffSuccess(statusCode) {
        if (statusCode == '200' || statusCode == '780' || statusCode == '774' || statusCode == '756' || statusCode == '754' || statusCode == '753' || statusCode == '711' || statusCode == '726') {
            return true;
        }
        return false;
    }

    unSubscribe(subs) {
        if (subs && typeof subs === 'object' && typeof subs.unsubscribe === 'function') {
            subs.unsubscribe();
        } else {
            if (subs && subs.length) {
                subs.forEach((localSub) => {
                    this.unSubscribe(localSub);
                });
            } else if (subs && typeof subs === 'object') {
                Object.keys(subs).forEach(key => {
                    this.unSubscribe(subs[key]);
                });
            } else {
                console.log('Not sure what it is...', subs);
            }
        }
    }
    /*Commomn function to POST FILE form data */

    uploadWithProgress(url, data) {
        return this.http.post(this.appConfig.host + url, data, {
            reportProgress: true,
            observe: 'events',
        }).pipe(map(res => res as {}), catchError(this.handleError));
    }

    upload(url, data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.appConfig.host + url, data)
                .pipe(map(res => res as {}), catchError(this.handleError))
                .subscribe((res: any) => {
                    if (res && res.uploadStatus == 'Success') {
                        resolve(res);
                    } else {
                        reject((res && res.errorDesc) ? res.errorDesc : this.commonStrings.http_error);
                    }
                }, error => {
                    reject(this.commonStrings.http_error);
                });
        });
    }

    appendCommonParameters(data) {
        if (!data) {
            data = {
                applicationId: (this.cache.user && this.cache.user.applicationId ? this.cache.user.applicationId : 0),
            };
        } else {
            if (data.applicationId == undefined) {
                data.applicationId = this.cache.user && this.cache.user.applicationId ? this.cache.user.applicationId : 0;
            }
        }

        if (data.applicationId == 0) {
            delete data.applicationId;
        }
        if (data.applicationId == 0) {
            delete data.applicationId;
        }

        return data;
    }

    getApi(url, params, headers) {
        if (!headers || !headers['Content-Type']) {
            headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        }
        params = this.appendCommonParameters(params);

        return new Promise((resolve, reject) => {
            this.http.get(this.appConfig.host + url, { params: params, headers: headers })
                .pipe(map(res => res as {}), catchError(this.handleError))
                .subscribe(res => {
                    resolve(res);
                }, error => {
                    reject(this.commonStrings.http_error);
                });
        });
    }

    postApi(url, data, headers) {
        if (!headers || !headers.get('Content-Type')) {
            headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        }
        data = this.appendCommonParameters(data);

        return new Promise((resolve, reject) => {
            this.http.post(this.appConfig.host + url, JSON.stringify(data), { headers: headers })
                .pipe(map(res => res as {}), catchError(this.handleError))
                .subscribe(res => {
                    resolve(res);
                }, error => {
                    reject(this.commonStrings.http_error);
                });
        });

    }

    postApiCancellable(url, data, headers) {
        if (!headers || !headers.get('Content-Type')) {
            headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        }

        data = this.appendCommonParameters(data);

        return this.http.post(this.appConfig.host + url, JSON.stringify(data), { headers: headers })
            .pipe(map(res => res as {}), catchError(this.handleError));
    }

    /* Common methods for customer data fetch/save*/

    handleError(error: any): Observable<Response> {
        console.error(error);
        if (error.status === 401 || error.status === 0) {
            return observableThrowError(error.message);
        } else {
            return observableThrowError(this.commonStrings.http_error);

        }
    }

    b64toBlob(b64Data, contentType) {
        contentType = contentType;
        const sliceSize = 512;

        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    /* Download first file from ARRAY */

    downloadToBrowser(fileResp) {
        if (fileResp && fileResp.file && fileResp.file[0]) {
            if (navigator.msSaveBlob) { // IE 10+
                navigator.msSaveBlob(this.b64toBlob(fileResp.file[0], fileResp.mimeType[0]), fileResp.fileName[0]);
            } else {
                const element = document.createElement('a');
                element.setAttribute('href', 'data:' + fileResp.mimeType[0] + ';base64,' + encodeURIComponent(fileResp.file[0]));
                element.setAttribute('download', fileResp.fileName[0]);
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            }
        }
    }
    /* Download Single file from OBJECT */

    downloadFileToBrowser(fileResp) {
        if (fileResp && fileResp.file) {
            if (navigator.msSaveBlob) { // IE 10+
                navigator.msSaveBlob(this.b64toBlob(fileResp.file, fileResp.mimeType), fileResp.fileName);
            } else {
                const element = document.createElement('a');
                element.setAttribute('href', 'data:' + fileResp.mimeType + ';base64,' + encodeURIComponent(fileResp.file));
                element.setAttribute('download', fileResp.fileName);
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            }
        }
    }

    viewFileDocument(fileResp) {
        const fileObj = fileResp.resultMap ? fileResp.resultMap : fileResp;
        if (fileObj && fileObj.base64PdfFile) {
            const file = new Blob([this.b64toBlob(fileObj.base64PdfFile, fileObj.mimeType), fileObj.fileName], { type: fileObj.mimeType });
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                return;
            } else {
                const objectUrl = URL.createObjectURL(file);
                window.open(objectUrl);
            }
        }
    }
}
