import { Injectable, Inject } from '@angular/core';

import { APP_CONFIG } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';
import { Cache } from './cache.service';
import { ApiService } from './api.service';

@Injectable()
export class DocumentsService {

    appConfig: IAppConfig;
    documents: any;
    valid: number = 0;
    allowedFiles: any[];

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
        private cache: Cache,
        private apiService: ApiService) {

        this.appConfig = appConfig;
        this.valid = 0;
        this.allowedFiles = ['pdf', 'png', 'jpeg', 'jpg'];

    }

    validateFileToUpload(files) {
        if (!files[0]) {
            return this.apiService.commonStrings.file_extention;
        }
        let fileSize = files[0].size / 1000000;
        let fileName = files[0].name.split('.')[0];
        let extention = files[0].name.split('.').pop().toLowerCase();

        if (fileSize > 10) {
            return files[0].name + '(' + fileSize + ')' + this.apiService.commonStrings.file_size;
        } else if (this.allowedFiles.indexOf(extention) == -1) {
            return this.apiService.commonStrings.file_extention;
        }
        return false;
    }

    getBase64File(file) {
        return new Promise((resolve, reject) => {

            var reader = new FileReader();
            reader.onload = (e)  => {
                let reader:any = e.target;
                resolve(reader.result);
            }
            reader.readAsDataURL(file);
        });
    }


    /*This common upload funtion fpr CJ and Grid DO NOT change upload URL */
    uploadFile(fileToUpload: File, data) {
        const formData: FormData = new FormData();
        formData.append('applicationId', this.cache.user && this.cache.user.applicationId ? this.cache.user.applicationId : 0);
        formData.append('file', fileToUpload, fileToUpload.name);

        let dataKeys = Object.keys(data);
        for (let prop of dataKeys) {
            formData.append(prop, data[prop]);
        }

        return this.apiService.uploadWithProgress(this.appConfig.endpoints.uploadDocument, formData);
    }

    deleteFile(data) {
        return new Promise((resolve, reject) => {

            this.apiService.postApi(this.appConfig.endpoints.deleteDocument, data, null)
                .then((res: any) => {
                    if (res) {
                        if (res.deleteStatus == 'Success') {
                            resolve(res.data);
                        } else {
                            reject(res.errorDesc);
                        }
                    } else {
                        reject(this.apiService.commonStrings.http_error);
                    }
                }, error => {
                    console.log('Oooops!' + error);
                    reject(this.apiService.commonStrings.http_error);
                });
        });
    }

    downloadFile(docId) {
        let data = { "adId": docId };
        return new Promise((resolve, reject) => {
            this.download(this.appConfig.endpoints.downloadDocument, data)
                .then(res => {
                    resolve(res);
                }, error => {
                    console.log('Oooops!' + error);
                    reject(error);
                });
        });
    }

    /** Common functions to call download API */
    download(url, data) {
        return new Promise((resolve, reject) => {

            if (this.apiService.downloadDisabled) {
                reject(this.apiService.commonStrings.download_disabled);
                return;
            }
            this.apiService.postApi(url, data, null)
                .then((resp: any) => {
                    if (resp) {
                        if ((resp.saveStatus === 'Success' || resp.Status == 'Success') || (resp.file || resp.resultMap)) {
                            resolve(resp);
                        } else if (resp.Exception) {
                            reject(resp);
                        } else if (resp.errorDesc) {
                            reject(resp.errorDesc);
                        } else {
                            reject(this.apiService.commonStrings.download_error);
                        }
                    } else {
                        reject(this.apiService.commonStrings.http_error);
                    }
                }, error => {
                    console.log('Oooops!' + error);
                    reject(this.apiService.commonStrings.http_error);
                });
        });
    }
}
