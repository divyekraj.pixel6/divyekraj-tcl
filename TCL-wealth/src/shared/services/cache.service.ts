import { Injectable } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';

import { User } from '../models/user';

@Injectable()
export class Cache{

    loggedIn: boolean;
    user: any;

    constructor (private storage: SessionStorageService) {

        let cachedUser = this.get('user');

        if(!cachedUser){
            this.user = new User(null);
            console.log('New user created');
        }else{
            this.user = new User(cachedUser);
            console.log('User found...');
        }
    }

    set(key, val) {
        let valString: string;
        if (typeof val == 'object') {
            valString = JSON.stringify(val);
        }else{
            valString = val;
        }
        this.storage.store(key, valString);
    }

    get(key) {
        let val = this.storage.retrieve(key)
        if (typeof val == 'string') {
            let valObj = JSON.parse(val);
            if (typeof valObj == 'object') {
                return valObj;
            }
        }
        return val;
    }
    clear(key) {
        return this.storage.clear(key);
    }
}
