import { NativeDateAdapter } from '@angular/material/core';
import * as moment from 'moment';

export class DateService {

    /* NGX plugin related date functions */
    static transformDate(inp: any) {
        if (inp) {
            if (inp.formatted) {
                return inp.formatted;
            } else if (inp.date && inp.month && inp.year) {
                return inp.date + '-' + inp.month + '-' + inp.year;
            } else if (inp.date && inp.date.day) {
                return inp.date.day + '-' + inp.date.month + '-' + inp.date.year;
            }
        }
        return '';
    }

    static getSinceUntilDate(years, months, days) {

        let d;
        if (years > 0) {
            d = moment().add(years, 'years');
        } else {
            d = moment().subtract((0 - years), 'years');
        }

        if (months > 0) {
            d = d.add(months, 'months');
        } else {
            d = d.subtract((0 - months), 'months');
        }

        if (days > 0) {
            d = d.add(days, 'days');
        } else {
            d = d.subtract((0 - days), 'days');
        }
        return {
            year: Number(d.format('YYYY')),
            month: Number(d.format('M')),
            day: Number(d.format('D'))
        };
    }

    static getDOBDefaultMonth(pastYear) {
        const currentDay = new Date();
        let month: any = currentDay.getMonth() + 1;

        if (month <= 9) { month = '0' + month; }

        return month + '/' + (pastYear ? (currentDay.getFullYear() - pastYear) : currentDay.getFullYear());
    }

    /* Generic functions to create array of moths,year, days */
    static getYearInRange(disableUpto) {
        const date = new Date();
        const minYear = date.getFullYear() - 100;
        const maxYear = date.getFullYear() - disableUpto;
        const years = new Array();
        for (let i = maxYear; i >= minYear; i--) {
            years.push(i);
        }
        return years;
    }

    static getMonthsArray() {
        return [
            { label: 'Jan', value: 1 },
            { label: 'Feb', value: 2 },
            { label: 'Mar', value: 3 },
            { label: 'Apr', value: 4 },
            { label: 'May', value: 5 },
            { label: 'June', value: 6 },
            { label: 'July', value: 7 },
            { label: 'Aug', value: 8 },
            { label: 'Sept', value: 9 },
            { label: 'Oct', value: 10 },
            { label: 'Nov', value: 11 },
            { label: 'Dec', value: 12 }
        ];
    }
    static getDatesInRange() {
        const dateSelect = new Array();
        for (let i = 1; i <= 31; i++) {
            dateSelect.push(i);
        }
        return dateSelect;
    }

    static to2digit(n: number) {
        return ('00' + n).slice(-2);
    }

    static getMonthLabel(month) {
        const monthLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        return monthLabels[month];
    }

    static getSinceUntilJSDate(years, months, days) {
        const data = DateService.getSinceUntilDate(years, months, days);
        return new Date(data.year, data.month - 1, data.day);
    }
}

export const MY_FORMATS = {
    parse: {
        dateInput: 'YYYY/MMM',
    },
    display: {
        dateInput: 'input'
    },
};

export class MonthYearDateAdapter extends NativeDateAdapter {
    format(date: Date, displayFormat: any): string {
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();

        if (displayFormat == 'input') {
            return DateService.getMonthLabel(month - 1) + ', ' + year;
        } else {
            return DateService.to2digit(day) + '/' + DateService.to2digit(month) + '/' + year;
        }
    }
}
