import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

import { ConstantsService } from '../services/constants.service';

@Pipe({
    name: 'formatSrNo'
})
export class FormatSrNoPipe implements PipeTransform {
    transform(value: any): any {
        value = value.toString();
        if (value) {
            let arr = value.split("");
            let format = '0' + arr[0] + '/' + '0' + arr[1] ;
            return format;
        }
    }
}

@Pipe({ name: 'tabkeys' })
export class TabKeyPipe implements PipeTransform {
    transform(value): any {
        let keys = [];

        for (let key in value) {
            keys.push(key);
        }

        return keys;
    }
}

@Pipe({ name: 'city' })
export class CityPipe implements PipeTransform {

    constructor(private constantsService: ConstantsService) {
    }

    parse(value: string): string {
        let cityString = '';
        this.constantsService.constants.cities.forEach(item => {
            if (item.value == value) {
                cityString = item.key;
            }
        }, this);
        return cityString;
    }

    transform(value: string): string {
        let cityString = '';

        this.constantsService.constants.cities.forEach(item => {

            if (item.key == value) {
                cityString = item.value;
            }
        }, this);
        return cityString;
    }
}

@Pipe({ name: 'state' })
export class StatePipe implements PipeTransform {

    constructor(private constantsService: ConstantsService) {
    }

    parse(value: string): string {
        let stateString = '';
        this.constantsService.constants.states.forEach(item => {
            if (item.value == value) {
                stateString = item.key;
            }
        }, this);
        return stateString;
    }

    transform(value: string): string {
        let stateString = '';

        this.constantsService.constants.states.forEach(item => {

            if (item.key == value) {
                stateString = item.value;
            }
        }, this);
        return stateString;
    }
}
@Pipe({name: 'slugify'})
export class SlugifyPipe implements PipeTransform {
  transform(input: string): string {
    return input.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '');            // Trim - from end of text
  }
}

@Pipe({
    name: 'safe'
})
export class SafePipe implements PipeTransform {

    constructor(protected sanitizer: DomSanitizer) {
    }

    public transform(value: any, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
        switch (type) {
            case 'html': return this.sanitizer.bypassSecurityTrustHtml(value);
            case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
            case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
            case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);

            default: throw new Error(`Invalid safe type specified: ${type}`);
        }
    }
}
