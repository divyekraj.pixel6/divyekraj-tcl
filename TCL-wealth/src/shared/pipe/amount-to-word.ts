import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'amountToWords'
})
export class AmountToWordsPipe implements PipeTransform {

    transform(amount: any): any {

        if (!amount) {
            return;
        }

        let amountStrings = new Array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen', 'Twenty');

        amountStrings[30] = 'Thirty';
        amountStrings[40] = 'Forty';
        amountStrings[50] = 'Fifty';
        amountStrings[60] = 'Sixty';
        amountStrings[70] = 'Seventy';
        amountStrings[80] = 'Eighty';
        amountStrings[90] = 'Ninety';

        amount = amount.toString();

        let amountLength: any = amount.length;
        let amountInWords: any = "";

        if (amountLength <= 9) {

            let newArray: any = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
            let digitArray: any = new Array();

            for (let i = 0; i < amountLength; i++) {
                digitArray[i] = amount.substr(i, 1);
            }

            for (let i = 9 - amountLength, j = 0; i < 9; i++ , j++) {
                newArray[i] = digitArray[j];
            }

            for (let i = 0, j = 1; i < 9; i++ , j++) {
                if (i == 0 || i == 2 || i == 4 || i == 7) {
                    if (newArray[i] == 1) {
                        newArray[j] = 10 + parseInt(newArray[j]);
                        newArray[i] = 0;
                    }
                }
            }

            amount = "";

            for (let i = 0; i < 9; i++) {

                if (i == 0 || i == 2 || i == 4 || i == 7) {
                    amount = newArray[i] * 10;
                } else {
                    amount = newArray[i];
                }

                if (amount != 0) {
                    amountInWords += amountStrings[amount] + " ";
                }

                if ((i == 1 && amount != 0) || (i == 0 && amount != 0 && newArray[i + 1] == 0)) {
                    amountInWords += "Crores ";
                }

                if ((i == 3 && amount != 0) || (i == 2 && amount != 0 && newArray[i + 1] == 0)) {
                    amountInWords += "Lakhs ";
                }

                if ((i == 5 && amount != 0) || (i == 4 && amount != 0 && newArray[i + 1] == 0)) {
                    amountInWords += "Thousand ";
                }

                if (i == 6 && amount != 0 && (newArray[i + 1] != 0 && newArray[i + 2] != 0)) {
                    amountInWords += "Hundred and ";
                } else if (i == 6 && amount != 0) {
                    amountInWords += "Hundred ";
                }

            }

            amountInWords = amountInWords.split("  ").join(" ");
        }

        return amountInWords;

    }

}
