export interface IAppConfig {
  endpoints: any;
  lookups: any;
  host: string;
  customerlookups: any;
  autosaveTimeout: any;
  restrictedChars: any;
  restrictedCharsRegEx: any;
  restrictedAllSpecialCharsRegEx: any;
}
