import { InjectionToken } from '@angular/core';
import { IAppConfig } from './iapp.config';

export let APP_CONFIG = new InjectionToken('app.config');

export const AppCustomerConfig: IAppConfig = {
    autosaveTimeout: 45000,

    /*While adding any new KeyCode below make sure to update regex too*/
    restrictedChars: [49, 52, 54, 56, 188, 190],
    restrictedCharsRegEx: '[!\\$\\^*<>]',
    restrictedAllSpecialCharsRegEx: '[!\\$\\^\'*<>_@#$%&()=+{};:,./?"|~`\-]',

    host: 'http://lab.thinkoverit.com/api/pixiebank/',
    endpoints: {
        logout: 'logout.php',
        getOtp: 'getOTP.php',
        verifyOtp: 'verifyOTP.php',

        getUserDetails: 'getUserDetails.php',

        pincodeDetails: 'lookup/getDetailsByZipCode.php',

        getPurposeOfLoan: 'getPuroseOfLoan.php',
        savePurposeOfLoan: 'savePurposeOfLoan.php',

        getLoanDetails: 'getLoanDetails.php',
        saveLoanDetails: 'saveLoanDetails.php',

        getPersonalDetails: 'getPersonalDetails.php',
        savePersonalDetails: 'savePersonalDetails.php',

        getProfessionalDetail: 'getProfessionalDetails.php',
        saveProfessionalDetail: 'saveProfessionalDetails.php',

        getLoanOfferDetails: 'getLoanOfferDeatils.php',
        saveLoanOfferDetails: 'saveLoanOfferDeatils.php',

    },
    lookups: {
        loanTypes: 'lookup/getPurposeOfLookUp.php',
        getIndustry: 'lookup/getIndustry.php',
        getSubIndustry: 'lookup/getSubIndustry.php',
        getSubindustryOccuption: 'lookup/getSubindustryOccuption.php',

        states: 'lookup/getStateLookup.php',
        cities: 'lookup/getCityLookup.php',
        lookupByName: 'lookup/getLookupByLookupName.php'

    },
    customerlookups: {
        loanPurpose: 'PURPOSE OF LOAN',
        residentialStatus: 'RESIDENTIAL STATUS',
        ownedBy: 'OWNED OR RENTED',
        maritalStatus: 'MARITAL STATUS',
        education: 'EDUCATION',
        addressProof: 'GOVERNMENTID',
        gender: 'GENDER',
        citizenShip: 'NATIONALITY',
        religion: 'RELIGION'
    }
};
