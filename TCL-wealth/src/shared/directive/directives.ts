import { Directive, Inject, HostListener, ElementRef } from '@angular/core'
import { NgControl } from '@angular/forms';

import { APP_CONFIG } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';

import { FormatSrNoPipe } from '../pipe/common.pipe';
/* numberOnly alphaOnly alphaWithSpaces alphaNumericOnly alphaNumericWithSpecialsOnly expression numberWithSlashOnly alphaNumericWithQuamma*/

@Directive({
	selector: '[numberOnly]',
	inputs: ['length', 'cropZero'],
})
export class NumberOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private cropZero: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.cropZero = true;
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		if (this.cropZero) {
			newValue = initalValue.replace(/^0(0+)?/g, '0');
		}
		newValue = newValue.replace(/[^0-9]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
    @HostListener('keyup', ['$event']) onKeyPressUp(event) {

        if(this.el.nativeElement.value.length >= this.length){
            if (event.srcElement.nextElementSibling) {
                event.srcElement.nextElementSibling.focus();
            }
        }
        if (event.keyCode == 8 && event.srcElement.previousElementSibling) {
            event.srcElement.previousElementSibling.focus();
        }
    }
}


@Directive({
	selector: '[floatNumberOnly]',
	inputs: ['integer', 'fraction'],
})
export class FloatNumberOnlyDirective {
	appConfig: IAppConfig;

	private integer: number;
	private fraction: number;


	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {		
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;		
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9.]*/g, '');					

		if (this.integer && newValue.integer > this.integer) {
			newValue = newValue.substring(0, this.integer)
		}
				
		if(this.fraction) newValue = newValue.replace(/[^0-9]*/g, '');
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
    @HostListener('keyup', ['$event']) onKeyPressUp(event) {

        if(this.el.nativeElement.value.integer >= this.integer){
            if (event.srcElement.nextElementSibling) {
                event.srcElement.nextElementSibling.focus();
            }
        }
        if (event.keyCode == 8 && event.srcElement.previousElementSibling) {
            event.srcElement.previousElementSibling.focus();
        }
    }
 
}


@Directive({
	selector: '[alphaOnly]',
	inputs: ['length', 'upperOnly'],
})
export class AlphaOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedAllSpecialCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^a-zA-Z]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}
@Directive({
	selector: '[alphaWithSpaces]',
	inputs: ['length'],
})
export class AlphaWithSpacesDirective {
	appConfig: IAppConfig;
	private length: number;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedAllSpecialCharsRegEx, "g");
		newValue = newValue.replace(re, '');

		newValue = newValue.replace(/^\s/g, '');
		newValue = newValue.replace(/[^a-zA-Z\s]*/g, '');
		newValue = newValue.replace(/\s\s/g, ' ');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[limitLength]',
	inputs: ['length'],
})
export class LimitLengthDirective {
	appConfig: IAppConfig;
	private length: number;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('blur', ['$event.target.value']) onBlur(value) {
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		value = value.replace(re, '');
		this.control.control.setValue(value);
	}
	@HostListener('keydown', ['$event']) onKeyPress(event) {

		let e = <KeyboardEvent>event;

		if (e.shiftKey && this.appConfig.restrictedChars.indexOf(e.keyCode) !== -1) {
			e.preventDefault();
		}
		if (this.length && this.el.nativeElement.value.length >= this.length && e.keyCode != 8) {
			e.preventDefault();
		}
	}
}

@Directive({
	selector: '[alphaNumericOnly]',
	inputs: ['length', 'upperOnly']
})
export class AlphaNumericOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;

		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9a-zA-Z]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[alphaNumericWithDashDotOnly]',
	inputs: ['length', 'upperOnly']
})
export class AlphaNumericWithDashDotOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;

		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9a-zA-Z._]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[alphaNumericWithSpaceOnly]',
	inputs: ['length', 'upperOnly']
  })
  export class AlphaNumericWithSpaceOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
	  private el: ElementRef,
	  private control: NgControl) {
	  this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
	  const initalValue = this.el.nativeElement.value;
	  let newValue = initalValue;
	  var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
	  newValue = newValue.replace(re, '');

	  newValue = newValue.replace(/^\s/g, '');
	  newValue = newValue.replace(/[^0-9a-zA-Z\s]*/g, '');
	  newValue = newValue.replace(/\s\s/g, ' ');


	  if (this.length && newValue.length > this.length) {
		newValue = newValue.substring(0, this.length)
	  }
	  if (this.upperOnly) newValue = newValue.toUpperCase();
	  this.el.nativeElement.value = newValue;
	  this.control.control.setValue(newValue);

	  if (initalValue !== this.el.nativeElement.value) {
		event.stopPropagation();
	  }
	}
  }

@Directive({
	selector: '[alphaNumericWithSpecialsOnly]',
	inputs: ['length', 'upperOnly']
})
export class AlphaNumericWithSpecialsOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9a-zA-Z-._@]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[alphaNumericWithSpecialsAndSpaceOnly]',
	inputs: ['length', 'upperOnly']
})
export class AlphaNumericWithSpecialsAndSpaceOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');

		newValue = newValue.replace(/^\s/g, '');
		newValue = newValue.replace(/[^0-9a-zA-Z.,;:&\[\](){}\/\\'\-\s]*/g, '');
		newValue = newValue.replace(/\s\s/g, ' ');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[numberWithSlashOnly]',
	inputs: ['length'],
})
export class NumberWithSlashOnlyDirective {
	appConfig: IAppConfig;

	private length: number;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9.\/]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[password]',
	inputs: ['length'],
})
export class PasswordDirective {
	appConfig: IAppConfig;

	private length: number;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/\s/g, '');
		newValue = newValue.replace(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{8,}$/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[alphaNumericWithPipe]',
	inputs: ['length', 'upperOnly'],
})
export class AlphaNumericWithQuammaDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
		private el: ElementRef,
		private control: NgControl) {
		this.appConfig = appConfig;
	}
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9a-zA-Z|]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
    selector: '[date]',
    inputs: ['length']
})

export class DateFormatterDirective {

    constructor(private el: ElementRef,
                public format: FormatSrNoPipe) {
    }

    @HostListener('keydown', ['$event']) onKeyPress(event) {
        var key = event.keyCode;
        if (this.el.nativeElement.value.length === 2 || this.el.nativeElement.value.length === 5) {
            this.el.nativeElement.value += '/';
        }

        if (key === 8) { /* If backspace is pressed*/
            if (this.el.nativeElement.value.length == 3 || this.el.nativeElement.value.length == 6) {
                /*if next char to be removed is /' remove last two characters from input value*/
                this.el.nativeElement.value = this.el.nativeElement.value.substr(0, this.el.nativeElement.value.length - 1);
            }
            /*remove last character*/
            this.el.nativeElement.value = this.el.nativeElement.value.substr(0, this.el.nativeElement.value.length);
        }
        /*if key pressed is not number or input got date*/
        else if (!((key > 47 && key < 58) || (key > 95 && key < 106))
                || this.el.nativeElement.value.length === 10) {
                if (!(key == 9 || key == 37 || key == 39)) {
                    event.preventDefault(); //no nothing
                }
        }
       return;
    }

    @HostListener('input', ['$event']) onInputChange(event) {
        if (this.el.nativeElement.value.length === 2) {
            let twoDigit = this.el.nativeElement.value;
            if (twoDigit > 31 && twoDigit <= 99) {
               let formated = this.format.transform(twoDigit)
                this.el.nativeElement.value = formated;
            }
        }
    }

}
