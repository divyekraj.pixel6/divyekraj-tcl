﻿import { Directive, Inject, HostListener, ElementRef, OnInit } from '@angular/core';
import { NgControl } from "@angular/forms";
import { Subscription } from 'rxjs';


import { IndianCurrencyPipe } from '../pipe/currency.pipe';

@Directive({ selector: '[indianCurrencyFormatter]', inputs: ['fraction', 'length', 'negative'] })
export class IndianCurrencyFormatterDirective implements OnInit {

    private el: HTMLInputElement;
    private fraction: number;
    private length: number;
    private negative: boolean;
    private totalLength: number;
    private valueSubscription: any;
    private DECIMAL_SEPARATOR: string;

    constructor(private elementRef: ElementRef,
        private control: NgControl,
        private currencyPipe: IndianCurrencyPipe) {

        this.DECIMAL_SEPARATOR = '.';
        this.el = this.elementRef.nativeElement;
    }

    ngOnInit() {
        this.fraction = this.fraction ? this.fraction : 0;
        this.length = this.length ? this.length : 8;
        this.negative = this.negative ? this.negative : false;

        if (this.fraction > 0) {
            this.totalLength = Number(this.length) + Number(this.fraction);
        } else {
            this.totalLength = Number(this.length);
        }
        //console.log('ngOnInit currency', this.control.name);
        //if (this.negative) this.totalLength += Number(1);

        this.valueSubscription = this.control.control.valueChanges.subscribe((value: string) => {
            //console.log('valueSubscription currency', this.control.name, value);

            if (value) {
                let formatted = this.currencyPipe.addCommas(value.toString());
                this.el.value = formatted;
            }
            this.valueSubscription.unsubscribe();
        });

    }

    @HostListener('input', ['$event']) onInputChange(event) {

        const initalValue = this.el.value;
        let newValue = initalValue;
        let shiftCursorBy = 0;

        let start = this.el.selectionStart;
        let end = this.el.selectionEnd;

        if (this.fraction > 0) {
            newValue = initalValue.replace(/[^0-9.]*/g, '');

            let [integer, decimal = ''] = newValue.split(this.DECIMAL_SEPARATOR);

            let enteredLength = integer.length + decimal.length;

            if (newValue.indexOf(this.DECIMAL_SEPARATOR) != -1) {
                if (this.fraction && decimal.length > this.fraction) {
                    // First check fraction size
                    newValue = integer + this.DECIMAL_SEPARATOR + decimal.substring(0, this.fraction);
                } else if (this.length && integer.length > this.length) {
                    // Now check integer size     
                    newValue = integer.substring(0, this.length) + this.DECIMAL_SEPARATOR + decimal;
                } else if (this.totalLength && enteredLength > this.totalLength) {
                    // Now check total length with fraction and decimal point    
                    newValue = newValue.substring(0, this.totalLength + 1);
                }
            } else if (newValue.indexOf(this.DECIMAL_SEPARATOR) == -1 && this.length && enteredLength > this.length) {
                // Else check length without fraction only
                newValue = newValue.substring(0, this.length);
            }
        } else {
            newValue = initalValue.replace(/[^0-9]*/g, '');
            // Check Integer length only
            if (this.length && newValue.length > this.length) {
                newValue = newValue.substring(0, this.length)
            }
        }


        // This will add commas and keep fraction as it is...
        newValue = this.currencyPipe.addCommas(newValue);

        let parsed = this.currencyPipe.parse(newValue, this.fraction);

        if (initalValue.length < newValue.length) {
            shiftCursorBy = 1;
        } else if (initalValue.length > newValue.length && start != 0) {
            shiftCursorBy = -1;
        }

        this.control.valueAccessor.writeValue(newValue);
        this.el.setSelectionRange(start + shiftCursorBy, end + shiftCursorBy);

        this.control.control.setValue(parsed, {
            emitEvent: false,
            emitModelToViewChange: false,
            emitViewToModelChange: false
        });

        if (initalValue !== this.el.value) {
            event.stopPropagation();
        }
    }

}
