import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Alert, AlertType } from '../models/alert';

@Component({
    moduleId: module.id,
    template: `<mat-dialog-content>
                    <div class="alert-wrapper">
                        <a class="close close-icon" (click)="closePopup()"><i class="material-icons md-24 md-dark" >cancel</i></a>
                        <div class="alert {{ cssClass(alert) }} " [innerHtml]="alert.message"></div>
                    </div>
                </mat-dialog-content>`,
    styles: [`
                .alert-wrapper {
                    position: relative;
                    text-align: center;
                    min-height: 100px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    padding: 20px 0;
                }
                .alert-wrapper .close {
                    position: absolute;
                    right: 0px;
                    top: 13px;
                    right: -8px;
                    -webkit-transform: translateY(-50%);
                    transform: translateY(-50%);
                    cursor: pointer;
                    color: #000;
                }
                .alert {
                    margin-bottom: 10px;
                    line-height: 1.7;
                }
                .alert-danger {
                    color: #f44336;
                }
                .alert-success {
                    color: #60b008;
                }
    `]
})

export class AlertComponent {
    alert: Alert;

    constructor(public dialogRef: MatDialogRef<AlertComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        this.alert = data;
    }

    ngOnInit() {
    }
    closePopup() {
        this.dialogRef.close();
    }
    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }

        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return 'alert alert-success';
            case AlertType.Error:
                return 'alert alert-danger';
            case AlertType.Info:
                return 'alert alert-info';
            case AlertType.Warning:
                return 'alert alert-warning';
        }
    }
}
