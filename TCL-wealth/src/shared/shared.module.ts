import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';
import { MdePopoverModule } from '@material-extended/mde';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng2Webstorage } from 'ngx-webstorage';
import { AppMaterialModule } from './material.module';
import { ClickOutsideModule } from 'ng4-click-outside';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

import { LoaderComponent } from './components/loader.component';
import { TimerComponent } from './components/timer.component';

import { Error404Component } from './components/error404/error-404.component';

import { PhonePipe } from './pipe/phone.pipe';
import { IndianCurrencyPipe } from './pipe/currency.pipe';
import { FormatSrNoPipe, TabKeyPipe, StatePipe, CityPipe, SafePipe, SlugifyPipe} from './pipe/common.pipe';

import { BrowserDirective } from './directive/browser.directive';
import { AlertComponent } from './directive/alert.directive';

import { PhoneFormatterDirective, MobileFormatterDirective } from './directive/phone-formatter.directive';
import { IndianCurrencyFormatterDirective } from './directive/currency-formatter.directive';
import {
	NumberOnlyDirective,
	NumberWithSlashOnlyDirective,
	AlphaOnlyDirective,
	AlphaNumericOnlyDirective,
	AlphaNumericWithDashDotOnlyDirective,
	AlphaWithSpacesDirective,
	LimitLengthDirective,
	AlphaNumericWithSpecialsOnlyDirective,
	AlphaNumericWithSpaceOnlyDirective,
	AlphaNumericWithSpecialsAndSpaceOnlyDirective,
	AlphaNumericWithQuammaDirective,
    PasswordDirective,
    DateFormatterDirective
} from './directive/directives';
import { AmountToWordsPipe } from './pipe/amount-to-word';

@NgModule({
	imports: [
		FormsModule,
		CommonModule,
		Ng2Webstorage,
		CdkTableModule,
		MdePopoverModule,
		AppMaterialModule,
		ClickOutsideModule,
		ReactiveFormsModule,
		NgxMyDatePickerModule.forRoot()
	],
	declarations: [
		AlertComponent,
    LoaderComponent,
    TimerComponent,
		Error404Component,
		CityPipe,
		PhonePipe,
		StatePipe,
		FormatSrNoPipe,
		TabKeyPipe,
		IndianCurrencyPipe,
		BrowserDirective,
		AlphaOnlyDirective,
		NumberOnlyDirective,
		LimitLengthDirective,
		PhoneFormatterDirective,
		MobileFormatterDirective,
		AlphaWithSpacesDirective,
		AlphaNumericOnlyDirective,
		AlphaNumericWithDashDotOnlyDirective,
		NumberWithSlashOnlyDirective,
		IndianCurrencyFormatterDirective,
		AlphaNumericWithSpecialsOnlyDirective,
		AlphaNumericWithSpaceOnlyDirective,
		AlphaNumericWithSpecialsAndSpaceOnlyDirective,
		AlphaNumericWithQuammaDirective,
        PasswordDirective,
        DateFormatterDirective,
        SafePipe,
        SlugifyPipe,
        AmountToWordsPipe
	],
	exports: [
		FormsModule,
		CommonModule,
		Ng2Webstorage,
		CdkTableModule,
		MdePopoverModule,
		AppMaterialModule,
		ClickOutsideModule,
		ReactiveFormsModule,
		NgxMyDatePickerModule,
		AlertComponent,
        LoaderComponent,
        TimerComponent,
		Error404Component,
		CityPipe,
		PhonePipe,
		StatePipe,
		FormatSrNoPipe,
		TabKeyPipe,
		IndianCurrencyPipe,
		BrowserDirective,
		AlphaOnlyDirective,
		NumberOnlyDirective,
		LimitLengthDirective,
		PhoneFormatterDirective,
		MobileFormatterDirective,
		AlphaWithSpacesDirective,
		AlphaNumericOnlyDirective,
		AlphaNumericWithDashDotOnlyDirective,
		NumberWithSlashOnlyDirective,
		IndianCurrencyFormatterDirective,
		AlphaNumericWithSpecialsOnlyDirective,
		AlphaNumericWithSpaceOnlyDirective,
		AlphaNumericWithSpecialsAndSpaceOnlyDirective,
		AlphaNumericWithQuammaDirective,
        PasswordDirective,
        DateFormatterDirective,
        SafePipe,
        SlugifyPipe,
        AmountToWordsPipe
	],
	providers: [
		CityPipe,
		PhonePipe,
		StatePipe,
		FormatSrNoPipe,
		TabKeyPipe,
        IndianCurrencyPipe,
        SafePipe,
        SlugifyPipe,
        AmountToWordsPipe
	],
    entryComponents: [ AlertComponent ],

})
export class SharedModule {

}
