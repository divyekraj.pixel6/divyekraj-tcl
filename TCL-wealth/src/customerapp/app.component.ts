import { Component } from '@angular/core';

import { AlertService } from '../shared/services/alert.service';
import { Cache } from '../shared/services/cache.service';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {

    constructor(public alert: AlertService,
                public cache: Cache) {

    }
    ngOnInit()    { console.log(`onInit`); }
    //ngDoCheck()    { console.log(`ngDoCheck`); }
    ngAfterContentInit()    { console.log(`ngAfterContentInit`); }
    //ngAfterContentChecked()    { console.log(`ngAfterContentChecked`); }
    ngAfterViewInit()    { console.log(`ngAfterViewInit`); }
    //ngAfterViewChecked()    { console.log(`ngAfterViewChecked`); }

    ngOnDestroy() { console.log(`onDestroy`); }

}
