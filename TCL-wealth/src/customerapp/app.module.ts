import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserIdleModule } from 'angular-user-idle';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgxWindowTokenModule } from 'ngx-window-token';

import { AppRoutingModule } from './app.routing.module';
import { SharedModule } from '../shared/shared.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header.component';
import { FooterComponent } from './core/footer.component';
import { LoginComponent } from './account/login.component';

import { TermsDialogComponent } from '../shared/components/terms-dialog/terms-dialog.component';

import { UserService } from '../shared/services/user.service';
import { APP_CONFIG, AppConfig } from '../shared/config/app.config';
import { ApiService } from '../shared/services/api.service';
import { AlertService } from '../shared/services/alert.service';
import { CustomValidators, ConfirmValidParentMatcher, FormSubmittedMatcher } from '../shared/services/custom-validators.service';
import { Cache } from '../shared/services/cache.service';
import { ConstantsService } from '../shared/services/constants.service';
import { TokenInterceptor } from '../shared/services/token.interceptor.service';
import { DocumentsService } from '../shared/services/documents.service';
import { GTMService } from '../shared/services/gtm.service';

import { CustomerService } from './services/customer.service';
import { HomeComponent } from './home/home.component';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        LoginComponent,
        TermsDialogComponent,
        HomeComponent
    ],
    imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        SharedModule,
        AppRoutingModule,
        NgxWindowTokenModule,
        UserIdleModule.forRoot({ idle: 300, timeout: 10, ping: 10 })
    ],

    providers: [
        UserService,
        DocumentsService,
        Cache,
        ApiService,
        CustomValidators,
        ConfirmValidParentMatcher,
        FormSubmittedMatcher,
        AlertService,
        ConstantsService,
        CustomerService,
        GTMService,
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
    ],
    entryComponents: [TermsDialogComponent],
    bootstrap: [AppComponent]
})

export class AppCustomerModule { }
