import { Component, Inject, ViewChild, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatTabChangeEvent } from '@angular/material';
import { MatDialog } from '@angular/material';
import { WINDOW } from 'ngx-window-token';
import { DOCUMENT } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

import { Cache } from '../../shared/services/cache.service';
import { CustomValidators, FormSubmittedMatcher } from '../../shared/services/custom-validators.service';
import { UserService } from '../../shared/services/user.service';
import { AlertService } from '../../shared/services/alert.service';
import { ApiService } from '../../shared/services/api.service';

import { TermsDialogComponent } from '../../shared/components/terms-dialog/terms-dialog.component';
import { TimerComponent } from '../../shared/components/timer.component';

import { CustomerService } from '../../customerapp/services/customer.service';

@Component({
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {

    @ViewChild('resendT') resendTimerEl: TimerComponent;
    @ViewChild('resendTResume') resendTimerResumeEl: TimerComponent;


    loginFormGroup: FormGroup;

    formSubmitted: boolean;
    formPanSubmitted: boolean;
    isRequestFromPL: boolean;

    mobileText: string;
    existingUser: number;
    step: number;

    otpRefId: any;
    _window: any;
    offerDetails: any;
    otpError: any;

    private valueChangesSubscriptions: Subscription[];

    @ViewChild('firstName') firstNameElement: ElementRef;
    @ViewChild('mobileNumber') mobileNumberElement: ElementRef;
    @ViewChild('otp') otpElement: ElementRef;
    @ViewChild('resumeotp') resumeotpElement: ElementRef;

    constructor(@Inject(WINDOW) _window,
        @Inject(DOCUMENT) private document: any,
        private fb: FormBuilder,
        public userService: UserService,
        private cache: Cache,
        private alert: AlertService,
        private router: Router,
        private apiService: ApiService,
        private customerService: CustomerService,
        public formSubmittedMatcher: FormSubmittedMatcher,
        public dialog: MatDialog,
    ) {

        this._window = _window;
        this.createForm();
        this.step = 1;
        this.existingUser = 0;
        this.otpError = '';
        this.formSubmitted = false;
        this.formPanSubmitted = false;
        this.valueChangesSubscriptions = new Array();
    }
    get loginform() { return this.loginFormGroup.controls; }
    createForm() {
        this.loginFormGroup = this.fb.group({
            mobileNumber: ['', [Validators.required, CustomValidators.mobileValidator]],
            firstName: ['', [Validators.required, CustomValidators.fullNameValidator]],
            middleName: ['', CustomValidators.fullNameValidator],
            lastName: ['', [Validators.required, CustomValidators.fullNameValidator]],
            email: ['', [Validators.required, CustomValidators.emailValidator]],
            otp: ['', [Validators.required]]
        });
    }
    ngAfterViewInit() {
        this.firstNameElement.nativeElement.focus();
    }
    ngOnInit() {
        let localSub: any;
        localSub = this.loginFormGroup.get('otp').valueChanges.subscribe(status => {
            this.otpError = '';
        });
        this.valueChangesSubscriptions.push(localSub);

        localSub = this.mobileNumber.valueChanges.subscribe(status => {
            this.otpError = '';
        });
        this.valueChangesSubscriptions.push(localSub);

    }

    ngOnDestroy() {
        this.apiService.unSubscribe(this.valueChangesSubscriptions);
    }

    changeLoginTab(event: MatTabChangeEvent) {
        this.loginFormGroup.get('otp').reset();
        if (event.index == 0) {
            this.existingUser = 0;
            this.step = 1;
            setTimeout(() => { this.firstNameElement.nativeElement.focus(); }, 0);
        } else {
            this.existingUser = 1;
            this.step = 1;
            this.mobileNumber.reset();
            setTimeout(() => { this.mobileNumberElement.nativeElement.focus(); }, 0);
        }
    }

    goAsGuest() {
        this.existingUser = 0;
        this.otpError = '';
    }

    goBack() {
        this.step = 1;

        this.loginFormGroup.get('otp').reset();
        this.otpError = '';
    }

    get mobileNumber() {
        return this.loginFormGroup.get('mobileNumber');
    }

    getOtp() {
        if (!this.mobileNumber.value || !this.mobileNumber.valid || this.formSubmitted) {
            return;
        }
        this.formSubmitted = true;

        this.loginFormGroup.get('otp').reset();
        this.mobileText = this.mobileNumber.value.toString().substring(6, 10);

        const postData = {
            mobileNumber: this.mobileNumber.value.toString(),
            appId: 0
        };

        this.userService.getOtp(postData).then((resp: any) => {
            if (resp.status.statusCode == '716' || resp.status.statusCode == '703') {
                this.otpError = resp.status.statusMessage;
                this.formSubmitted = false;
            } else if (this.apiService.isDropOff(resp)) {
                this.afterDropOff(resp);
            } else {
                this.otpRefId = resp.refId;
                this.step = 2;

                if (this.existingUser == 0) {
                    setTimeout(() => {
                        this.otpElement.nativeElement.focus();
                        this.resendTimerEl.startOtpTimer();
                    });
                } else {
                    setTimeout(() => {
                        this.otpElement.nativeElement.focus();
                        this.resendTimerResumeEl.startOtpTimer();
                    });
                }
                this.formSubmitted = false;
            }
        }, (error) => {
            this.alert.error(error);
            this.formSubmitted = false;
        });
    }

    verifyOtp() {

        if ((this.existingUser == 0 && (!this.loginFormGroup.valid)) ||
            (this.existingUser == 1 && (!this.mobileNumber.valid || !this.loginFormGroup.get('otp').valid)) ||
            this.formSubmitted) {
            return;
        }

        const otp = this.loginFormGroup.get('otp').value;
        const postData = {
            mobile: this.mobileNumber.value.toString(),
            otp: otp,
            refId: this.otpRefId,
            isResumeApplication: (this.existingUser == 1 ? true : false),
            appId: 0
        };
        this.formSubmitted = true;
        this.userService.verifyOtp(postData).then((resp: any) => {
            console.log(resp);

            if (resp.status.statusCode == '716' || resp.status.statusCode == '703') {
                this.otpError = resp.status.statusMessage;
            } else if (resp.status.statusCode == '783') {
                this.userService.logout();
                this.step = 1;
                this.existingUser = 0;
            } else if (this.apiService.isDropOff(resp)) {
                this.afterDropOff(resp);
            } else {
                this.cache.user.mobileNumber = resp.mobileNumber;

                this.cache.user.existingUser = this.existingUser;
                this.cache.user.firstName = resp.firstName;
                this.cache.user.middleName = resp.middleName;
                this.cache.user.lastName = resp.lastName;
                this.cache.user.email = resp.email;

                this.cache.set('user', this.cache.user);

                this.cache.user.firstName = resp.firstName;
                this.afterLoginRedirect(resp.stage);

                // if (this.existingUser == 0) {
                //     this.saveCustomerDetails(resp.stage);
                // } else {
                //     this.cache.user.firstName = resp.firstName;
                //     this.getCustomerDetails(resp.stage);
                // }

            }
            this.formSubmitted = false;
        }, (error) => {
            this.alert.error(error);
            this.loginFormGroup.get('otp').reset();
            this.formSubmitted = false;
        });
    }

    resumeApp() {
        this.existingUser = 1;
        this.step = 1;
    }

    goToPreApprovedStage() {
        this.step = 3;
    }

    submitPreApprove() {
        this.step = 4;
    }

    // getCustomerDetails(stage) {
    //     this.customerService.getUserDetails().then((resp: any) => {

    //         this.cache.user.firstName = resp.firstName;
    //         this.cache.user.middleName = resp.middleName;
    //         this.cache.user.lastName = resp.lastName;
    //         this.cache.user.email = resp.email;
    //         this.cache.user.mobileNumber = this.mobileNumber.value;

    //         this.cache.set('user', this.cache.user);

    //         this.afterLoginRedirect(stage);

    //     }, (error) => {
    //         this.userService.logout();
    //         this.existingUser = 0;
    //         this.step = 1;
    //         this.formSubmitted = false;
    //     });
    // }

    // saveCustomerDetails(newStage) {

    //     this.cache.user.firstName = this.loginFormGroup.get('firstName').value;
    //     this.cache.user.middleName = this.loginFormGroup.get('middleName').value;
    //     this.cache.user.lastName = this.loginFormGroup.get('lastName').value;
    //     this.cache.user.email = this.loginFormGroup.get('email').value;
    //     this.cache.user.mobileNumber = this.mobileNumber.value.toString();
    //     this.cache.set('user', this.cache.user);

    //     this.customerService.saveCustomerDetails(this.loginFormGroup.value).then((resp: any) => {
    //         this.getCustomerDetails(newStage);
    //     }, (error) => {
    //         this.alert.error(error);
    //     });
    // }

    afterLoginRedirect(stage) {

        const url = this.customerService.getUrlAfterLoginRedirect(stage);

        if (stage == 'POST_OTP' && this.existingUser == 0) {
            this.router.navigate(['application']);
        } else if (stage == 'POST_OTP' && this.existingUser == 1) {
            this.existingUser = 0;
            this.formSubmitted = false;
            this.userService.logout();
        } else {
            this.router.navigate(url);
        }
    }

    goToStep(step) {
        this.step = step;
    }


    openTermsDialog() {
        const dialogRef = this.dialog.open(TermsDialogComponent, {
            width: '800px'
        });
    }

    afterDropOff(res) {
        this.customerService.dropOffError = res.status.statusMessage;
        this.customerService.dropOffErrorReason = res.status.dropOffPoint;
        this.customerService.dropOffStatusCode = res.status.statusCode;
        this.router.navigateByUrl('/thank-you');
    }
}
