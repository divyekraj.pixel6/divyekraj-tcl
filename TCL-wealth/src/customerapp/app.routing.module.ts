import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Error404Component } from '../shared/components/error404/error-404.component';
import { LoginComponent } from './account/login.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'apply-now', component: LoginComponent },
    { path: 'home', component: HomeComponent },
    {
        path: 'application',
        loadChildren: './customerapp/application/customer.module#CustomerModule',
        data: { preload: true }
    },
    { path: '**', component: Error404Component }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { useHash: false })
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})

export class AppRoutingModule { }
