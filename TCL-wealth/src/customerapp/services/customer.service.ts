import { Injectable, Inject } from '@angular/core';

import { APP_CONFIG } from '../../shared/config/app.config';
import { IAppConfig } from '../../shared/config/iapp.config';

import { ApiService } from '../../shared/services/api.service';
import { Cache } from '../../shared/services/cache.service';
import { ConstantsService } from '../../shared/services/constants.service';

import { PersonalDetails, ProfessionalDetail, InitialOfferDetails } from '../../shared/models/customer';

@Injectable()
export class CustomerService {

    appConfig: IAppConfig;

    dropOffError: string;
    dropOffStatusCode: string;
    dropOffErrorReason: string;

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
        private apiService: ApiService,
        private constantsService: ConstantsService) {

        this.appConfig = appConfig;
        this.resetData();

        this.dropOffError = '';
        this.dropOffStatusCode = '';
        this.dropOffErrorReason = '';
    }

    resetData() {
    }

    getPincodeDetails(data) {
        return this.apiService.postApiCancellable(this.appConfig.endpoints.pincodeDetails, data, null);
    }

    getUserDetails() {

        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getUserDetails, null).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveCustomerDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveCustomerDetails, data).then(resp => {
                resolve(resp);
            }, error => {
                reject(error);
            });
        });
    }

    getPurposeOfLoan() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getPurposeOfLoan, null).then((resp: any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    savePurposeOfLoan(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.savePurposeOfLoan, data).then((resp: any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getLoanDetails() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getLoanDetails, null).then((resp: any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveLoanDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveLoanDetails, data).then((resp: any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getPersonalDetails() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getPersonalDetails, null).then((resp: any) => {
                resolve(new PersonalDetails(resp));
            }, (error) => {
                reject(error);
            });
        });
    }

    savePersonalDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.savePersonalDetails, data).then((resp: any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }


    getProfessionalDetail() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getProfessionalDetail, null).then((resp: any) => {
                resolve(new ProfessionalDetail(resp));
            }, (error) => {
                reject(error);
            });
        });
    }

    saveProfessionalDetail(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveProfessionalDetail, data).then((resp: any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }
  
    getLoanOfferDetails() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getLoanOfferDetails, null).then((resp: any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveLoanOfferDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveLoanOfferDetails, data).then((resp: any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    get(url, data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(url, data, null).then((resp: any) => {
                if (resp) {
                    resolve(resp);
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    save(url, data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(url, data, null).then((resp: any) => {
                if (resp) {
                    if (resp.statusCode && resp.statusCode == 200) {
                        resolve(resp);
                    } else if (resp.statusMessage) {
                        reject(resp.statusMessage);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    getTellUsFormConstants() {
        this.constantsService.getLookupByName(this.appConfig.customerlookups.residentialStatus);
        this.constantsService.getLookupByName(this.appConfig.customerlookups.ownedBy);
        this.constantsService.getLookupByName(this.appConfig.customerlookups.maritalStatus);
        this.constantsService.getLookupByName(this.appConfig.customerlookups.education);
        this.constantsService.getLookupByName(this.appConfig.customerlookups.addressProof);
        this.constantsService.getLookupByName(this.appConfig.customerlookups.gender);
        this.constantsService.getLookupByName(this.appConfig.customerlookups.citizenShip);
    }

   
    getUrlAfterLoginRedirect(stage) {
        if (stage == 'POST_OTP') {
            return ['apply-now'];
        } else if (stage == 'LOAN_PURPOSE') {
            return ['/application/', 'loan-purpose'];
        } else if (stage == 'TELL_US') {
            return ['/application/', 'tell-us'];
        }
        return ['/application/', 'loan-purpose'];
    }
}
