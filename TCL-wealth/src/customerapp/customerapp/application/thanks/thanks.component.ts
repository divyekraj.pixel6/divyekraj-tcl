import { Component, OnInit } from '@angular/core';

import { UserService } from '../../../../shared/services/user.service';
import { ApiService } from '../../../../shared/services/api.service';

import { CustomerService } from '../../../services/customer.service';

@Component({
    selector: 'app-thanks',
    templateUrl: './thanks.component.html'
})

export class AppThanksComponent implements OnInit {
    constructor(public userService: UserService,
        public apiService: ApiService,
        public customerService: CustomerService) {
    }

    ngOnInit() {
        this.userService.logout();
    }
}
