import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoPractiseComponent } from './demo-practise.component';

describe('DemoPractiseComponent', () => {
  let component: DemoPractiseComponent;
  let fixture: ComponentFixture<DemoPractiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoPractiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoPractiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
