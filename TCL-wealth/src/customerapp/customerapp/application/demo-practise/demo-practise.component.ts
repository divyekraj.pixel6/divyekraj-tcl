import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormSubmittedMatcher, CustomValidators } from '../../../../shared/services/custom-validators.service';


@Component({
  selector: 'app-demo-practise',
  templateUrl: './demo-practise.component.html',
  styleUrls: ['./demo-practise.component.sass']
})
export class DemoPractiseComponent implements OnInit {
  demoPractiseForm: FormGroup
  formSubmitted: boolean;

  constructor(private fb: FormBuilder,
    public formSubmittedMatcher: FormSubmittedMatcher,
    private router: Router) {
    this.formSubmitted = false;
  }

  ngOnInit() {
    this.createForm()
  }

  get form() { return this.demoPractiseForm.controls }
  createForm() {
    this.demoPractiseForm = this.fb.group({
      fname: ['', [Validators.required, CustomValidators.fullNameValidator]],
      mname: ['', [Validators.required, CustomValidators.fullNameValidator]],
      lname: ['', [Validators.required, CustomValidators.fullNameValidator]],
      email: ['', [Validators.required, CustomValidators.emailValidator]],
      mobileno: ['', [Validators.required, CustomValidators.mobileValidator]],
      panno: ['', [Validators.required, CustomValidators.panValidator]]
    })


  }

  next() {
    this.router.navigate(['application', 'current-address'])
  }



}
