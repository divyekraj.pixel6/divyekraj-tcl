import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-document-upload',
  templateUrl: './document-upload.component.html',
  styleUrls: ['./document-upload.component.sass']
})
export class DocumentUploadComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  url: string
  imageChange(event) {
    if (event.target.files) {
      var r = new FileReader()
      r.readAsDataURL(event.target.files[0])
      r.onload = (e: any) => {
        this.url = e.target.result
      }
    }
  }

}
