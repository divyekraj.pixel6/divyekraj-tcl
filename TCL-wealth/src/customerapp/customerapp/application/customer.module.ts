import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/shared.module';
import { AppMaterialModule } from '../../../shared/material.module';

import { AppCustomerRoutingModule } from './app-customer-routing.module';

import { ApplicationComponent } from './application.component';
import { LoanPurposeComponent } from './loan-purpose/loan-purpose.component';
import { TellUsAboutYouComponent } from './tell-us/tell-us.component';
import { InitialOfferComponent } from './initial-offer/initial-offer.component';
import { AppThanksComponent } from './thanks/thanks.component';
import { ProfessionalDetailComponent } from './professional-detail/professional-detail.component';
import { AadhaarConsentComponent } from './aadhaar-consent/aadhaar-consent.component';
import { AadhaarUploadComponent } from './aadhaar-upload/aadhaar-upload.component';
import { CurrentAddressComponent } from './current-address/current-address.component';
import { DocumentUploadComponent } from './document-upload/document-upload.component';
import { DemoPractiseComponent } from './demo-practise/demo-practise.component';


@NgModule({
    imports: [
        SharedModule,
        AppMaterialModule,
        AppCustomerRoutingModule,

    ],
    exports: [
    ],
    declarations: [
        ApplicationComponent,
        LoanPurposeComponent,
        TellUsAboutYouComponent,
        InitialOfferComponent,
        AppThanksComponent,
        ProfessionalDetailComponent,
        AadhaarConsentComponent,
        AadhaarUploadComponent,
        CurrentAddressComponent,
        DocumentUploadComponent,
        DemoPractiseComponent
    ],
    providers: [
    ],
    entryComponents: [],
})

export class CustomerModule {
    constructor() {
    }
}
