import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AadhaarConsentComponent } from './aadhaar-consent.component';

describe('AadhaarConsentComponent', () => {
  let component: AadhaarConsentComponent;
  let fixture: ComponentFixture<AadhaarConsentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AadhaarConsentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AadhaarConsentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
