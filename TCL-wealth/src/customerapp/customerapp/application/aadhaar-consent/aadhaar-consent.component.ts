import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
Router

@Component({
  selector: 'app-aadhaar-consent',
  templateUrl: './aadhaar-consent.component.html',
  styleUrls: ['./aadhaar-consent.component.sass']
})
export class AadhaarConsentComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  next() {
    this.router.navigate(['application', 'demo-practise'])
  }

}
