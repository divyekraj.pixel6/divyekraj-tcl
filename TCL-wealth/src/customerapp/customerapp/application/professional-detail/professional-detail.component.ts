import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { FormSubmittedMatcher, CustomValidators } from '../../../../shared/services/custom-validators.service';
import { IndianCurrencyPipe } from '../../../../shared/pipe/currency.pipe';
import { ApiService } from '../../../../shared/services/api.service';
import { AlertService } from '../../../../shared/services/alert.service';
import { Cache } from '../../../../shared/services/cache.service';
import { ConstantsService } from '../../../../shared/services/constants.service';

import { CustomerService } from '../../../services/customer.service';

@Component({
  selector: 'app-professional-detail',
  templateUrl: './professional-detail.component.html'  
})
export class ProfessionalDetailComponent implements OnInit {

	professionalDetailFormGroup: FormGroup;
    formSubmitted: boolean;
    dataLoader: boolean;
   

    constructor(private fb: FormBuilder,
		public formSubmittedMatcher: FormSubmittedMatcher,		       
        public currencyPipe: IndianCurrencyPipe,
        public customerService: CustomerService,
        public constantsService: ConstantsService,
        public apiService: ApiService,
        public alert: AlertService,
        private router: Router,
        private route: ActivatedRoute,
        public cache: Cache) { 
		
		this.createForm();       	
        this.formSubmitted = false;
        this.dataLoader = true;
        this.cache.user.screenName = 'Professional Detail Screen';
    }

    get form () { return this.professionalDetailFormGroup.controls; }

    createForm() {
        this.professionalDetailFormGroup = this.fb.group({
            organisationName: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(35), CustomValidators.companyNameValidator(35)]],
            organisationType: ['', Validators.required],
            monthlyTakeHomeSalary: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(7), CustomValidators.currencyValidator]], 
            monthlyObligations: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(7), this.monthlyOblValidator.bind(this), CustomValidators.currencyValidator]],            
            expCurrentOrganisation: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(2)]],
            totalExp: ['', [Validators.required, this.totalExpValidator.bind(this), Validators.minLength(1), Validators.maxLength(2)]]                                
        });
    }

    ngOnInit() {
        this.getProfessionalDetail();
	  
    }

    getProfessionalDetail() {
        this.dataLoader = true;
        this.customerService.getProfessionalDetail().then((resp: any) => {
            this.dataLoader = false;
            this.professionalDetailFormGroup.patchValue(resp);
        }, (error) => {
            this.dataLoader = false;
            this.alert.error(error);
        });
    }

	saveProfessionalDetail(){        
        if (this.formSubmitted || !this.professionalDetailFormGroup.valid) {
            return;
        }
        this.formSubmitted = true;
        const postData = this.professionalDetailFormGroup.getRawValue();
        this.customerService.saveProfessionalDetail(postData).then((resp: any) => {
            this.router.navigate(['application', 'initial-offer']);
            this.formSubmitted = false;
        }, (error) => {
            this.formSubmitted = false;
            this.alert.error(error);
        });   
    }


    monthlyOblValidator(controls: FormControl){
        if (!this.professionalDetailFormGroup){
          return null;
        }
        let salary = this.professionalDetailFormGroup.get('monthlyTakeHomeSalary').value;
        if (!controls.value) {
          return {
            required: true
          };
        } else if (controls.value > salary) {
          return {
            limit: true
          };
        }
        return null;
    }

 
    totalExpValidator(controls: FormControl){
        if (!this.professionalDetailFormGroup){
          return null;
        }
        let experience = this.professionalDetailFormGroup.get('expCurrentOrganisation').value;
        if (!controls.value) {
          return {
            required: true
          };
        } else if (controls.value < experience) {
          return {
            limit: true
          };
        }
        return null;
    }

}
