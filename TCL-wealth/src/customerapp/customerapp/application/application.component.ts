import { Component, OnInit, OnDestroy } from '@angular/core';

import { UserService } from '../../../shared/services/user.service';

import { CustomerService } from '../../services/customer.service';

@Component({
    templateUrl: './application.component.html'
})

export class ApplicationComponent implements OnInit, OnDestroy {

    showProgress: boolean;
    step: number;

    constructor(public customerService: CustomerService,
        private userService: UserService) {
        this.userService.startUserSession();
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

}
