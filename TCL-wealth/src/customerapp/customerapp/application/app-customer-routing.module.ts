import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ApplicationComponent } from './application.component';
import { LoanPurposeComponent } from './loan-purpose/loan-purpose.component';
import { TellUsAboutYouComponent } from './tell-us/tell-us.component';
import { ProfessionalDetailComponent } from './professional-detail/professional-detail.component';
import { InitialOfferComponent } from './initial-offer/initial-offer.component';
import { AppThanksComponent } from './thanks/thanks.component';
import { AadhaarConsentComponent } from './aadhaar-consent/aadhaar-consent.component';
import { AadhaarUploadComponent } from './aadhaar-upload/aadhaar-upload.component';
import { CurrentAddressComponent } from './current-address/current-address.component';
import { DocumentUploadComponent } from './document-upload/document-upload.component';
import { DemoPractiseComponent } from './demo-practise/demo-practise.component';

const customerRoutes: Routes = [
    {
        path: '', component: ApplicationComponent,
        children: [
            { path: '', redirectTo: 'aadhaar-consent', pathMatch: 'full' },
            { path: 'loan-purpose', component: LoanPurposeComponent },
            { path: 'aadhaar-consent', component: AadhaarConsentComponent },
            { path: 'aadhaar-upload', component: AadhaarUploadComponent },
            { path: 'current-address', component: CurrentAddressComponent },
            { path: 'document-upload', component: DocumentUploadComponent },
            { path: 'demo-practise', component: DemoPractiseComponent },
            { path: 'tell-us', component: TellUsAboutYouComponent },
            { path: 'professional-detail', component: ProfessionalDetailComponent },
            { path: 'initial-offer', component: InitialOfferComponent },
            { path: 'thank-you', component: AppThanksComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(customerRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppCustomerRoutingModule {
}
