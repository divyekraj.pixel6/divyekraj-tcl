import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { FormSubmittedMatcher } from '../../../../shared/services/custom-validators.service';
import { ApiService } from '../../../../shared/services/api.service';
import { AlertService } from '../../../../shared/services/alert.service';
import { Cache } from '../../../../shared/services/cache.service';

import { CustomerService } from '../../../services/customer.service';

@Component({
    selector: 'app-initial-offer',
    templateUrl: './initial-offer.component.html',
})
export class InitialOfferComponent implements OnInit {
    formSubmitted: boolean;
    dataLoader: boolean;
    initialOfferDetails: any;

    constructor(private router: Router,
        public formSubmittedMatcher: FormSubmittedMatcher,
        public customerService: CustomerService,
        public apiService: ApiService,
        public alert: AlertService,
        private fb: FormBuilder,
        public cache: Cache) {

        this.createForm();

        this.cache.user.screenName = 'Loan Offer Details Screen';

        this.dataLoader = true;
        this.formSubmitted = false;
        this.initialOfferDetails = {};
    }

    createForm() {

    }

    ngOnInit() {
        this.getLoanOfferDetails();
    }

    getLoanOfferDetails() {
        this.dataLoader = true;
        this.customerService.getLoanOfferDetails().then((resp: any) => {
            this.initialOfferDetails = resp;
            this.dataLoader = false;
        }, (error) => {
            this.dataLoader = false;
            this.alert.error(error);
        });
    }

    next() {
        if (this.formSubmitted || this.formSubmitted) {
            return;
        }
        this.formSubmitted = true;

        const postData = {};//this.initialOfferFromGroup.getRawValue();

        this.customerService.saveLoanOfferDetails(postData).then((resp: any) => {
            this.router.navigate(['application', 'thank-you']);
            this.formSubmitted = false;
        }, (error) => {
            this.formSubmitted = false;
            this.alert.error(error);
        });
    }

    revisedOffer() {

    }
}
