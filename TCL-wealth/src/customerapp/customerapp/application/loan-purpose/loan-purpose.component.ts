import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { FormSubmittedMatcher } from '../../../../shared/services/custom-validators.service';
import { ConstantsService } from '../../../../shared/services/constants.service';
import { AlertService } from '../../../../shared/services/alert.service';
import { IndianCurrencyPipe } from '../../../../shared/pipe/currency.pipe';
import { Cache } from '../../../../shared/services/cache.service';
import { GTMService } from '../../../../shared/services/gtm.service';

import { CustomerService } from '../../../services/customer.service';

@Component({
    selector: 'app-loan-pupose',
    templateUrl: './loan-purpose.component.html'
})
export class LoanPurposeComponent implements OnInit {
    loanPurposeFormGroup: FormGroup;

    step: number;
    loanLimits: any;
    tenureSliderStep: any;
    tenureMonthSliderStep: any;
    sliderStep: any;

    verify: boolean;
    formSubmitted: boolean;
    dataLoader: boolean;

    constructor(private router: Router,
        private fb: FormBuilder,
        public constantsService: ConstantsService,
        public gtmService: GTMService,
        public customerService: CustomerService,
        public alert: AlertService,
        public formSubmittedMatcher: FormSubmittedMatcher,
        public currencyPipe: IndianCurrencyPipe,
        private route: ActivatedRoute,
        public cache: Cache) {

        this.createForm();
        this.constantsService.getPurposeOfLookUp();

        this.loanLimits = { amountDetails: {maxAmount:100000, minAmount:10000}, tenures: {}, tenuresMonth: {} };

        this.sliderStep = 10;
        this.step = 2;
        this.dataLoader = true;

        this.route.queryParams.subscribe((params: any) => {
            if (params && params.step) {
                this.step = params.step;
            } else {
                this.step = 1;
                this.cache.user.screenName = 'Purpose Of Loan';
            }
            this.getPurposeOfLoan();

            if (this.step === 2) {
                this.getLoanDetails();
                this.cache.user.screenName = 'Loan Details Screen';
            }
        });

    }
    get form () { return this.loanPurposeFormGroup.controls; }

    ngOnInit() {

    }

    getPurposeOfLoan() {
        this.dataLoader = true;
        this.customerService.getPurposeOfLoan().then((resp: any) => {
            this.loanPurposeFormGroup.get('loanType').patchValue(resp.purposeOfLoan);
            this.dataLoader = false;
        }, (error) => {
            this.dataLoader = false;
            this.alert.error(error);
        });

    }

    savePurposeOfLoan(purposeKey, purposeText) {
        if (this.formSubmitted) {
            return;
        }
        this.formSubmitted = true;
        this.loanPurposeFormGroup.get('loanType').patchValue(purposeKey);

        this.gtmService.sendEvent({
            event: 'PL_Purpose',
            EventCategory: 'Personal Loan',
            EventAction: 'Purpose',
            EventLabel: purposeText,
            url: '/vpv/au-personal-loan/application/step-1/purpose'
        });
        this.customerService.savePurposeOfLoan({ purposeOfLoan: purposeKey }).then((resp: any) => {
            this.router.navigate(['application', 'loan-purpose'], { queryParams: { step: 2 } });
            this.getLoanDetails();
            this.formSubmitted = false;
        }, (error) => {
            this.formSubmitted = false;
            this.alert.error(error);
        });
    }

    getLoanDetails() {

        this.dataLoader = true;
        this.customerService.getLoanDetails().then((resp: any) => {
            this.loanPurposeFormGroup.patchValue(resp);
            this.dataLoader = false;

        }, (error) => {
            this.dataLoader = false;
            this.alert.error(error);
        });

    }

    selectLoanDuration(value) {

        this.loanPurposeFormGroup.get('loanDuration').patchValue(value);
        this.loanPurposeFormGroup.get('tenure').reset();
    }

    saveLoanDetails() {
        if (this.formSubmitted || !this.loanPurposeFormGroup.valid) {
            return;
        }

        this.formSubmitted = true;
        const postData = this.loanPurposeFormGroup.getRawValue();

        this.gtmService.sendEvent({
            event: 'PL_Amount-Tenure',
            EventCategory: 'Personal Loan',
            EventAction: 'Loan Amount and Tenure',
            EventLabel: 'Next',
            url: '/vpv/au-personal-loan/application/step-2/loan-amount-and-tenure'
        });

        this.customerService.saveLoanDetails(postData).then((resp: any) => {
            this.router.navigate(['application', 'tell-us']);
            this.formSubmitted = false;
        }, (error) => {
            this.formSubmitted = false;
            this.alert.error(error);
        });
    }

    loanValidator(controls: FormControl) {
        if (!this.loanPurposeFormGroup) { return null; }

        if (!controls.value) {
            return {
                required: true
            };
        } else if (controls.value < this.loanLimits.amountDetails.minAmount ||
            controls.value > this.loanLimits.amountDetails.maxAmount) {
            return {
                limit: true
            };
        }
        return null;
    }

    amountLengthLimit(value) {
        return parseInt(value).toString().length;
    }

    loanAmountBlur() {
        let value = this.loanPurposeFormGroup.get('loanAmount').value;

        if (!value) {
            return;
        }
        if (value < this.loanLimits.amountDetails.minAmount) {
            this.loanPurposeFormGroup.get('loanAmount').patchValue(this.loanLimits.amountDetails.minAmount);
        } else {
            if (value > this.loanLimits.amountDetails.maxAmount) {
                value = this.loanLimits.amountDetails.maxAmount;
                value = Math.round(value / 1000) * 1000;
                this.loanPurposeFormGroup.get('loanAmount').patchValue(value);
            }
        }
    }

    updateLoanAmount(event) {
        const control = <FormControl>this.loanPurposeFormGroup.get('loanAmount');
        control.setValue(this.currencyPipe.addCommas(event.value), {
            emitEvent: false,
            emitModelToViewChange: true,
            emitViewToModelChange: false
        });
        control.setValue(event.value, {
            emitEvent: false,
            emitModelToViewChange: false,
            emitViewToModelChange: false
        });

        this.setAmountSliderStep(event.value);
    }

    tenureValidator(controls: FormControl) {
        if (!this.loanPurposeFormGroup) { return null; }

        const value = controls.value;
        if (!value) {
            return {
                required: true
            };
        } else if (value < this.loanLimits.tenuresMonth.minTenure ||
            value > this.loanLimits.tenuresMonth.maxTenure) {
            return {
                limit: true
            };
        } else {
            const partial = value % this.tenureSliderStep;
            if (partial) {
                return {
                    step: true
                };
            }
        }
        return null;
    }

    tenureBlur() {
        const value = this.loanPurposeFormGroup.get('tenure').value;
        let selected = 0;
        if (!value) {
            return;
        }
        if (value < this.loanLimits.tenuresMonth.minTenure) {
            selected = this.loanLimits.tenures.minTenure;
        } else if (value > this.loanLimits.tenuresMonth.maxTenure) {
            selected = this.loanLimits.tenures.maxTenure;
        } else {
            const partial = value % this.tenureSliderStep;
            if (partial) {
                if (partial <= 6) {
                    selected = Math.floor(value / this.tenureSliderStep) * this.tenureSliderStep;
                } else {
                    selected = Math.ceil(value / this.tenureSliderStep) * this.tenureSliderStep;
                }
            }
        }
        this.loanPurposeFormGroup.get('tenure').patchValue(selected);
    }

    setAmountSliderStep(value) {
        if (value > 20000) {
            this.sliderStep = 5000;
        } else {
            this.sliderStep = 1000;
        }
    }

    setTenureSliderStep() {
        if (this.loanLimits.tenures.maxTenure > 12) {
            this.tenureSliderStep = 12;
        } else {
            this.tenureSliderStep = 6;
        }
    }

    setTenureMonthSliderStep() {
        if (this.loanLimits.tenuresMonth.maxTenure > 12) {
            this.tenureMonthSliderStep = 12;
        } else {
            this.tenureMonthSliderStep = 6;
        }
    }

    updateTenure(event) {
        this.loanPurposeFormGroup.get('tenure').patchValue(event.value);

        if (this.loanPurposeFormGroup.get('loanDuration').value == 1) {
            this.setTenureSliderStep();
        } else {
            this.setTenureMonthSliderStep();
        }
    }

    createForm() {
        this.loanPurposeFormGroup = this.fb.group({
            loanType: ['', Validators.required],
            loanAmount: ['', [this.loanValidator.bind(this)]],
            tenure: ['', [this.tenureValidator.bind(this)]],
            loanDuration: [2]
        });
    }
}
