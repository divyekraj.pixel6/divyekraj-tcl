import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AadhaarUploadComponent } from './aadhaar-upload.component';

describe('AadhaarUploadComponent', () => {
  let component: AadhaarUploadComponent;
  let fixture: ComponentFixture<AadhaarUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AadhaarUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AadhaarUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
