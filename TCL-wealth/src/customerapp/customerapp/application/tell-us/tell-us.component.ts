import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { debounceTime, switchMap, filter, tap } from 'rxjs/operators';

import { FormSubmittedMatcher, CustomValidators } from '../../../../shared/services/custom-validators.service';
import { DateService } from '../../../../shared/services/date-service';
import { ConstantsService } from '../../../../shared/services/constants.service';
import { ApiService } from '../../../../shared/services/api.service';
import { AlertService } from '../../../../shared/services/alert.service';
import { CustomerService } from '../../../services/customer.service';
import { Cache } from '../../../../shared/services/cache.service';
import { GTMService } from '../../../../shared/services/gtm.service';

@Component({
    selector: 'app-tell-us',
    templateUrl: './tell-us.component.html'
})

export class TellUsAboutYouComponent implements OnInit, OnDestroy {

    tellAboutFormGroup: FormGroup;
    formSubmitted: boolean;
    dataLoader: boolean;
    pinCodeLoder: boolean;
    valueChangesSubscriptions: Subscription[];
    public myDobDateOptions: INgxMyDpOptions;
    addressLength: number;    
    addressValue: string;


    constructor(private fb: FormBuilder,
        public formSubmittedMatcher: FormSubmittedMatcher,
        public constantsService: ConstantsService,
        private customerService: CustomerService,
        public apiService: ApiService,
        public gtmService: GTMService,
        public alert: AlertService,
        private router: Router,
        private route: ActivatedRoute,
        public cache: Cache) {

        this.createForm();
        this.customerService.getTellUsFormConstants();

        this.myDobDateOptions = {
            dateFormat: 'dd/mm/yyyy',
            disableSince: DateService.getSinceUntilDate(-21, 0, 0),
            disableUntil: DateService.getSinceUntilDate(-100, 0, 0)
        };
        this.dataLoader = true;
        this.formSubmitted = false;
        this.valueChangesSubscriptions = new Array();

        this.cache.user.screenName = 'Personal Details Screen';

    }
    get form () { return this.tellAboutFormGroup.controls; }

    createForm() {
        this.tellAboutFormGroup = this.fb.group({
            dob: ['', Validators.required],
            gender: ['', Validators.required],
            currentAddress: ['', [CustomValidators.addressValidator(255)]],
            currentCity: ['', [Validators.required, CustomValidators.cityValidator(50)]],
            currentState: ['', Validators.required],
            currentPinCode: ['', [Validators.required, CustomValidators.postalCodeValidator]],
            currentCountry: ['', Validators.required],
            residentialStatus: ['', Validators.required],
            currentAddressIs: ['', Validators.required],
            citizenShip: ['', Validators.required],
            maritalStatus: ['', Validators.required],
            educationQualification: ['', Validators.required],
            currentAddressProof: ['', Validators.required],
            currentAddressProofNumber: ['', Validators.required],
            noOfDependents: ['', Validators.required],
            currentAddrNoOfYears: ['', Validators.required],
            currentAddrNoOfMonths: ['', Validators.required],
        });
    }

    ngOnInit() {

        this.getPersonalDetails();

        this.onChanges();
    }

    onChanges() {
        let localSubs: any;

        localSubs = this.tellAboutFormGroup.get('currentAddressProof').valueChanges.subscribe(value => {
            if (value && value == 5) {
                this.tellAboutFormGroup.get('currentAddressProofNumber').setValidators([Validators.required, CustomValidators.passportValidator]);               
                this.tellAboutFormGroup.get('currentAddressProofNumber').updateValueAndValidity();
                this.addressLength = 15;
                this.addressValue = "Passport";
            } else if (value && value == 4) {
                this.tellAboutFormGroup.get('currentAddressProofNumber').setValidators([Validators.required, CustomValidators.voterCardValidator]);
                this.tellAboutFormGroup.get('currentAddressProofNumber').updateValueAndValidity();
                this.addressLength = 10;
                this.addressValue = "Voter card";
            } else if (value && value == 3) {
                this.tellAboutFormGroup.get('currentAddressProofNumber').setValidators([Validators.required, CustomValidators.aadharValidator]);
                this.tellAboutFormGroup.get('currentAddressProofNumber').updateValueAndValidity();
                this.addressLength = 12;
                this.addressValue = "Aadhar";
            } else if (value && value == 2) {
                this.tellAboutFormGroup.get('currentAddressProofNumber').setValidators([Validators.required, CustomValidators.electricityBillValidator]);
                this.tellAboutFormGroup.get('currentAddressProofNumber').updateValueAndValidity();
                this.addressLength = 10;
                this.addressValue = "Electricity bill";
            } else {
                this.tellAboutFormGroup.get('currentAddressProofNumber').clearValidators();
            }          
        });
        this.valueChangesSubscriptions.push(localSubs);        


        localSubs = this.tellAboutFormGroup.get('currentPinCode').valueChanges
            .pipe(
                debounceTime(200),
                filter(value => {
                    return this.tellAboutFormGroup.get('currentPinCode').valid;
                }),
                tap(value => this.pinCodeLoder = true),
                switchMap(value => this.customerService.getPincodeDetails({ zipCode: value })),
                tap(value => this.pinCodeLoder = false)
            ).subscribe((resp: any) => {
                if (resp.cityId && resp.cityName) {
                    this.tellAboutFormGroup.get('currentCity').patchValue(resp.cityName);
                    this.tellAboutFormGroup.get('currentCity').disable();
                }
                if (resp.stateId && resp.stateName) {
                    this.tellAboutFormGroup.get('currentState').patchValue(resp.stateName);
                    this.tellAboutFormGroup.get('currentState').disable();
                }
                if (resp.countryId && resp.countryName) {
                    this.tellAboutFormGroup.get('currentCountry').patchValue(resp.countryName);
                    this.tellAboutFormGroup.get('currentCountry').disable();
                }
            }, (error) => {
                this.alert.error(error);
            });
        this.valueChangesSubscriptions.push(localSubs);
    }

    getPersonalDetails() {
        this.dataLoader = true;
        this.customerService.getPersonalDetails().then((resp: any) => {
            this.dataLoader = false;
            if (resp.aadhaaradressproof) {
                this.tellAboutFormGroup.get('currentAddressProofNumber').patchValue(resp.aadhaaradressproof);
            }
            this.tellAboutFormGroup.patchValue(resp);
        }, (error) => {
            this.dataLoader = false;
            this.alert.error(error);
        });
    }

    clearAddress() {
        this.tellAboutFormGroup.get('currentAddress').reset();
        this.tellAboutFormGroup.get('currentCity').reset();
        this.tellAboutFormGroup.get('currentState').reset();
        this.tellAboutFormGroup.get('currentPinCode').reset();
        this.tellAboutFormGroup.get('currentCountry').reset();
        this.tellAboutFormGroup.get('residentialStatus').reset();
    }
    savePersonalDetails() {
        // call savePersonalDetails
        if (this.formSubmitted || !this.tellAboutFormGroup.valid) {
            return;
        }
        this.formSubmitted = true;
        const postData = this.tellAboutFormGroup.getRawValue();
        postData.dob = DateService.transformDate(postData.dob);


        this.customerService.savePersonalDetails(postData).then((resp: any) => {
            this.router.navigate(['application', 'professional-detail']);
            this.formSubmitted = false;
        }, (error) => {
            this.formSubmitted = false;
            this.alert.error(error);
        });
    }

    getDOBDefaultMonth(pastYear) {
        return DateService.getDOBDefaultMonth(pastYear);
    }

    dependentValidator(controls: FormControl) {
        if (!this.tellAboutFormGroup) { return null; }

        if (!controls.value) {
            return {
                required: true
            };
        } else if (controls.value > 3) {
            return {
                limit: true
            };
        }
        return null;
    }

    ngOnDestroy() {
        if (this.valueChangesSubscriptions) {
            this.apiService.unSubscribe(this.valueChangesSubscriptions);
        }
    }
}
