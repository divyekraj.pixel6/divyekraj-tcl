import { Component } from '@angular/core';

import { Cache } from '../../shared/services/cache.service';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html'
})

export class HeaderComponent {

    constructor(public cache: Cache) {

    }

}
