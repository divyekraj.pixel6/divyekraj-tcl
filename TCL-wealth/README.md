
## Overview

This is Sample Project for Loan application to Imaginary Bank. Its purpose is to make newcomers aware of all the custom components/directives/pipes we use in a typical angular project.
So run this project and explore code to understand the business logic and code structure which plays important role in making a modular optmized angular project.

## Development server

Run `npn run servecustomerapp` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `npm run buildcustomerapp` to build the project. The build artifacts will be stored in the `dist/customerapp/` directory. 

